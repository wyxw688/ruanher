<?php

use think\facade\Route;

Route::rule('api/:action','api/api/:action');

Route::get('code/:code', 'index/code');

Route::rule('app/:id','index/index/app/');