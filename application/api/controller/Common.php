<?php

namespace app\api\controller;

use think\Controller;
use think\facade\Request;
use HTTP_Request2;
use think\Db;
use think\exception\HttpException;

class Common extends Controller

{
    protected $request;

    public function initialize()
    {
        header('Access-Control-Allow-Origin:*');
        $this->page = input('?page') ? input('page') : 1;
        $this->limit = input('?limit') ? input('limit') : 10;
        $this->app = $this->getappinfos();
    }

    public function getappinfos()
    {
        $appinfo = db("app")->find(); 
        $appinfo += db("webset")->find();        
        $appinfo += db("appupdate")->order("time", 'desc')->find();
        return $appinfo;
    }
    /**
     * 解密字符串
     *
     * @param [type] $input
     * @param [type] $securityKey
     */
    public static function WeDecrypt($input, $securityKey)
    {
        $input = urldecode($input);
        $input = openssl_decrypt($input, 'AES-128-ECB', $securityKey);
        if (!$input) {
            return self::returnjson(400, "服务器错误");
        }
        return $input;
    }

    /**
     * 加密字符串
     *
     * @param [type] $input
     * @param [type] $securityKey
     */
    public static function WeDoctorEncrypt($input, $securityKey,$securityIv)
    {
        $inputArr = json_decode($input, true); //转为数组
        if (!is_array($inputArr) || empty($inputArr)) {
            return self::returnjson(400, "服务器错误");
        }
        $input = json_encode($inputArr, JSON_UNESCAPED_UNICODE); //转为json字符串
        //进行Aes加密
        // $data = openssl_encrypt($input, 'AES-128-ECB', $securityKey);
        return base64_encode(openssl_encrypt($input,"AES-128-CBC",$securityKey,OPENSSL_RAW_DATA,$securityIv));
        // return urlencode($data);
    }

    function returnjson($code = 200, $msg = "success", $data = [])
    {
        $result = [
            "code" => $code,
            "msg" => $msg,
            "data" => $data
        ];
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        die();
    }

    public function successjson($msg = "success", $data = [], $code = 200)
    {
        $result = [
            "code" => $code,
            "msg" => $msg,
            "data" => $data
        ];
        $result = json_encode($result, JSON_UNESCAPED_UNICODE);
        $result = $this->WeDoctorEncrypt($result, $this->app["signkey"],$this->app["signiv"]);
        return $result;
    }

    public function errorjson($msg = "error", $data = [], $code = 400)
    {
        return $this->successjson($msg, $data, $code);
    }

    public function getaddress($ip = '')
    {
        $request = new HTTP_Request2();
        $request->setUrl('http://opendata.baidu.com/api.php?query=' . $ip.'&co=&resource_id=6006&oe=utf8');
        $request->setMethod(HTTP_Request2::METHOD_GET);
        $request->setConfig(array(
            'follow_redirects' => TRUE
        ));
        try {
            $response = $request->send();
            if ($response->getStatus() == 200) {
                return $response->getBody();
            } else {
                return 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .
                    $response->getReasonPhrase();
            }
        } catch (HttpException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }
}
