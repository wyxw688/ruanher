<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
/**
 * 随机生成字符串
 *
 * @param [type] $length 位数
 * @return string
 */
function getRandChar($length)
{
    $str = null;
    $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
    $max = strlen($strPol) - 1;
    for ($i = 0; $i < $length; $i++) {
        $str .= $strPol[rand(0, $max)];
    }
    return $str;
}
//页面获取所有app的方法
function query($name)
{
    $res = db($name)->select();
    return $res;
}

function get_real_ip()
{
    $realip = NULL;
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipArray = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
        foreach ($ipArray as $rs) {
            $rs = trim($rs);
            if ($rs != 'unknown') {
                $realip = $rs;
                break;
            }
        }
    } else if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $realip = $_SERVER['HTTP_CLIENT_IP'];
    } else {
        $realip = $_SERVER['REMOTE_ADDR'];
    }
    preg_match("/[\d\.]{7,15}/", $realip, $match);
    $realip = !empty($match[0]) ? $match[0] : '0.0.0.0';
    return $realip;
}

function return_json($code = 200, $msg, $data = [])
{
    $result = [
        "code" => $code,
        "msg" => $msg,
        "data" => $data
    ];
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    die();
}

//订单号
function getorderNumber($orderNumberPrefix, $userid)
{
    $utimestamp = microtime(true);
    $timestamp = floor($utimestamp);
    $milliseconds = round(($utimestamp - $timestamp) * 1000); //获取当前时间戳的毫秒数
    return $orderNumberPrefix . $userid . date(preg_replace('`(?<!\\\\)u`', $milliseconds, 'YmdHisu'), $timestamp); //输出14位年月日时分秒+3位毫秒数
}

function scanFile($path)
{
    global $result;
    $files = scandir($path);
    foreach ($files as $file) {
        if ($file != '.' && $file != '..') {
            if (is_dir($path . '/' . $file)) {
                scanFile($path . '/' . $file);
            } else {
                $result[] = basename($file);
            }
        }
    }
    return $result;
}

function getDir()
{
    $dir = "../application/index/view/index/";
    $data = array();
    $result = scanFile($dir);
    foreach ($result as $key => $value) {
        array_push($data, substr($value, 0, -5));
    }
    return $data;
}

//数组查找
function inarraybyId($array,$userid)
{
    $flag = false;
    foreach ($array as $key => $value) {
        if ($value == $userid) {
            $flag = true;
        }
    }
    return $flag;
}
