<?php

namespace app\admin\model;

use app\admin\validate\Admin as ValidateAdmin;
use think\Model;

class Admin extends Model
{
    /**
     * 登录
     *
     * @param [type] $data
     */
    public function login($data)
    {
        //对数据进行校验
        $validate = new ValidateAdmin();
        if(!$validate->scene('login')->check($data)){
            return $validate->getError();
        }
        $result = $this->where("adminname",$data["adminname"])->find();
        if($result){
            if($result["adminpass"] == md5($data["adminpass"])){
                //修改当前用户的token,并后期使用token登录
                $admintoken = getRandChar(6);
                $this->where("id",$result["id"])->update(["admintoken"=>md5($admintoken.time())]);
                $rememberme = input("post.rememberme");
                if($rememberme == "on"){
                    //存入cookie，后期登录使用
                    $remembermedata = [
                        "rememberme" => 1,
                        "adminname" => $data["adminname"],
                        "admintoken" => $admintoken,
                    ];
                    cookie('remembermedata',$remembermedata,5*24*3600);
                }
                //存入session
                $admindata = [
                    "id" => $result["id"],
                    "adminname" => $result["adminname"],
                    "adminqq" => $result["adminqq"],
                    "nickname" => $result["nickname"]
                ];
                session("admin",$admindata);
                return 1;
            }else{
                return "账号或密码错误q";
            }
        }else{
            return "账号或密码错误";
        }
    }

    /**
     * 5天免登录
     *
     * @param [type] $data
     * @return bool
     */
    public function dologin($data)
    {
        $result = $this->where("adminname",$data["adminname"])->find();
        if($result){
            if($result["admintoken"] == $data["admintoken"]){
                //存入session
                $admindata = [
                    "id" => $result["id"],
                    "adminname" => $result["adminname"],
                    "adminqq" => $result["adminqq"],
                    "nickname" => $result["nickname"]
                ];
                session("admin",$admindata);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    //管理员信息
    public function adminInfo()
    {
        $adminid = session("admin.id");
        $result = $this->where("id",$adminid)->find();
        return $result;
    }

    //修改信息
    public function edit($data)
    {
        //对数据进行校验
        $validate = new ValidateAdmin();
        if(!$validate->scene('edit')->check($data)){
            return $validate->getError();
        }
        $adminid = session("admin.id");
        $this->where("id",$adminid)->update($data);
        //存入新的session
        $result = $this->where("id",$adminid)->find();
        $admindata = [
            "id" => $result["id"],
            "adminname" => $result["adminname"],
            "adminqq" => $result["adminqq"],
            "nickname" => $result["nickname"]
        ];
        session("admin",$admindata);
        return 1;
    }

    /**
     * key的操作
     *
     * @param [type] $code 1 生成新的key  2关闭key
     * @return void
     */

    public function editpass($data)
    {
        $adminid = session("admin.id");
        $validate = new ValidateAdmin();
        if(!$validate->scene('editpass')->check($data)){
            return $validate->getError();
        }
        $result = $this->where("id",$adminid)->find();
        $newpassword = md5($data["newpassword"]);
        $this->where("id",$adminid)->update(["adminpass"=>$newpassword]);
        return 1;
    }

}