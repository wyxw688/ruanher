<?php 

namespace app\admin\model;

use app\admin\validate\App as DateApp;
use think\Model;

class Appad extends Model
{
    //获取所有广告
    public function getallad($data)
    {
        $result = db('appad')
            ->order($data['sort'], $data['sortOrder'])
            ->limit($data["limit"])->page($data["page"])			
            ->select();
		$count = db('appad')
		->count();
        return json(["rows" => $result, "total" => $count]);
    }

	//新增广告
    public function addad($data)
    {
        $validate = new DateApp();
        if(!$validate->scene('addad')->check($data)){
            return $validate->getError();
        }
        $result = $this->allowField(true)->save($data);
        if($result > 0){
            return 1;
        }else{
            return "服务器错误";
        }
    }
	
	//删除广告
	public function deletead($id)
	{
	    if(!is_array($id)){
	        return "服务器错误";
	    }
	    foreach($id as $key=>$value){
	        $this->where("id",$value)->delete();
	    }
	    return 1;
	}
	
	//获取广告信息
	public function getadinfo($id)
	{
	    $result = $this->where("id",$id)->find();
	    return $result;
	}
	
	//修改广告
	public function editad($data)
	{	
		$validate = new DateApp();
		if(!$validate->scene('editad')->check($data)){
		    return $validate->getError();
		}
		$id = $data["id"];
		unset($data["id"]);
	    $result = $this->allowField(true)->save($data,["id"=>$id]);
	    if($result){
	        return 1;
	    }else{
	        return "服务器错误";
	    }
	}
}