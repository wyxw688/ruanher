<?php 

namespace app\admin\model;

use app\admin\validate\App as DateApp;
use think\Model;

class App extends Model
{
	//获取当前软件配置（登录注册、邮件配置、软件信息等）
    public function getapp($type)
    {
        $result = $this->find();
        $value = json_decode($result,true);
        return $value;
    }
	//获取已发布版本列表
	public function getappupdate($data)
	{
	    $result = db("appupdate")
	        ->where("versioncode", "like", "%{$data['versioncode']}%")
	        ->limit($data["limit"])->page($data["page"])
	        ->order($data["sort"], $data["sortOrder"])
	        ->select();
		$count = db("appupdate")
		    ->where("versioncode", "like", "%{$data['versioncode']}%")
		    ->limit($data["limit"])->page($data["page"])
		    ->order($data["sort"], $data["sortOrder"])
		    ->count();
	    return json(["rows" => $result, "total" => $count]);
	}

	//删除发布的版本
	public function deleteup($id)
    {
        if(!is_array($id)){
            return "服务器错误";
        }
        foreach($id as $key=>$value){
            db("appupdate")->where("id",$value)->delete();
        }
        return 1;
    }
	//编辑注册配置
	public function editzuce($data)
	{
	    $validate = new DateApp(); 
	    if (!$validate->scene('editzuce')->check($data)) {
	        return $validate->getError();
	    }
	    $result = $this->allowField(true)->save($data, ["id" => 1]);
	    return 1;
	}
	//编辑邮件配置
	public function editemail($data)
	{
	    $validate = new DateApp();
	    if (!$validate->scene('editemail')->check($data)) {
	        return $validate->getError();
	    }
	    $result = $this->allowField(true)->save($data, ["id" => 1]);
	    return 1;
	}
	//编辑软件配置
	public function editappus($data)
	{
	    $validate = new DateApp();
	    if (!$validate->scene('editappus')->check($data)) {
	        return $validate->getError();
	    }
	    $result = $this->allowField(true)->save($data, ["id" => 1]);
	    return 1;
	}
	//编辑支付配置
	public function editapppay($data)
	{
	    $validate = new DateApp();
	    if (!$validate->scene('editapppay')->check($data)) {
	        return $validate->getError();
	    }
	    $result = $this->allowField(true)->save($data, ["id" => 1]);
	    return 1;
	}
	//更新发布公告
	public function editappmsg($data)
	{
	    $validate = new DateApp();
	    if (!$validate->scene('editappmsg')->check($data)) {
	        return $validate->getError();
	    }
	    $result = $this->allowField(true)->save($data, ["id" => 1]);
	    return 1;
	}

}