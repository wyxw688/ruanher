<?php

namespace app\admin\model;

use app\admin\validate\Km as ValidateKm;
use think\Model;

class Km extends Model
{
    public function getkmlist($data)
    {
        $sort = isset($data["sort"]) ? $data["sort"] : 'id';
        $sortOrder = isset($data["sortOrder"]) ? $data["sortOrder"] : 'asc';
        $result = db("km")
            ->where("km", "like", "%{$data['km']}%")
            ->where("state", "like", "%{$data['state']}%")
            ->limit($data["limit"])->page($data["page"])
            ->order($sort, $sortOrder)
            ->select();
        $count = db("km")
            ->where("km", "like", "%{$data['km']}%")
            ->where("state", "like", "%{$data['state']}%")
            ->limit($data["limit"])->page($data["page"])
            ->order($sort, $sortOrder)
            ->count();
        return json(["rows" => $result, "total" => $count]);
    }

    public function deletekm($id)
    {
        if (!is_array($id)) {
            return "服务器错误";
        }
        foreach ($id as $key => $value) {
            $this->where("id", $value)->delete();
        }
        return 1;
    }

    public function add($data)
    {
        $validate = new ValidateKm();
        if (!$validate->scene('add')->check($data)) {
            return $validate->getError();
        }
        $adddata = [
            "money" => $data["money"],
            "viptime" => $data["viptime"]
        ];
        $resdata = [];
        for ($i = 0; $i < $data["num"]; $i++) {
            $km = getRandChar($data['length']);
            $rs = $this->where("km", $km)->find();
            if ($rs == null) {
                $resdata[$i] = $adddata;
                $resdata[$i]["km"] = $km;
                $resdata[$i]["creattime"] = date("Y-m-d H:i:s", time());
            }
        }
        $this->saveAll($resdata);
        return 1;
    }

    public function exportdownload($data, $type = 0)
    {
        $result = db("km")
            ->where("km", "like", "%{$data['km']}%")
            ->where("state", "like", "%{$data['state']}%")
            ->select();
        return $result;
    }
}
