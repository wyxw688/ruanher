<?php

namespace app\admin\model;

use app\admin\validate\Appdown as ValidateApp;
use think\Model;

class Appdown extends Model
{
    //获取所有软件
    public function getallapp($data)
    {
        $result = db('appdown')
			->alias("a")
			->join("appdownsort s", "s.id=a.appsortid")
			->join("user u", "u.id=a.appuserid")
            ->where("a.appname","like","%".$data["appname"]."%")
			->field("a.*,s.sortname,u.username")
            ->order($data['sort'], $data['sortOrder'])
            ->limit($data["limit"])->page($data["page"])			
            ->select();
		$count = db('appdown')
		->alias("a")
		->join("appdownsort s", "s.id=a.appsortid")
		->join("user u", "u.id=a.appuserid")
		->where("appname","like","%".$data["appname"]."%")
		->count();
        return json(["rows" => $result, "total" => $count]);
    }
	
	//删除软件
    public function deleteapp($id)
    {
        if(!is_array($id)){
            return "服务器错误";
        }
        foreach($id as $key=>$value){
            $this->where("id",$value)->delete();
        }
        return 1;
    }	

	// 审核软件
    public function editstate($id)
    {
        if(!is_array($id)){
            return "服务器错误";
        }
		$apppost=[
			"apppost" => 1
		];
        foreach($id as $key=>$value){
            $this->where("id",$value)->update($apppost);
        }
        return 1;
    }
	
	//一键审核软件
	public function editallstate()
	{	
		$apppost=[
			"apppost" => 1
		];
		$all = $this->where("apppost",'=',0)->update($apppost);
		if($all){
			return 1;
		}else{
			return "已无未审核软件";
		}
	}
	
	//新增软件
	public function addapp($data)
	{
	    $validate = new ValidateApp();
	    if(!$validate->scene('addapp')->check($data)){
	        return $validate->getError();
	    }
	    $apps = $this->where("appname",$data["appname"])->find();
	    if($apps){
	        return "已存在该软件！！";
	    }
	    $data['appuserid'] = db('user')->where('username',$data['appuser'])->value('id');
		$user = db('user')->where("username",$data["appuser"])->find();
		if(!$user){
			return "没有此用户！！";
		}	
		$tagArr = array_unique(explode(";",$data['apptag']));
		$tagIds = [];
		foreach ($tagArr as $tag) {
		    $id = db('apptag')->where('tag', $tag)->value('id');
		    if ($id) {
		        $tagIds[] = $id;
		    } else {
		        $tagIds[] = db('apptag')->insertGetId(['tag' => $tag]);
		    }
		}
		$data['apptagid'] = implode(',', array_map('strval', $tagIds));		
		$data['apptime'] = date('Y-m-d H:i:s');
		$data['apppost'] = 1;
	    $result = $this->allowField(true)->save($data);
	    if($result > 0){
	        return 1;
	    }else{
	        return "服务器错误";
	    }
	}
	
	//获取软件信息
	public function getappinfo($id)
	{
	    $result = $this->where("id",$id)->find();
		$result['appuser'] = db('user')->where('id',$result['appuserid'])->value('username');
		
		$tagIdArr = explode(",",$result['apptagid']);
		$tags = [];
		foreach ($tagIdArr as $tagid) {
			$tags[] = db('apptag')->where('id', $tagid)->value('tag');
		}
		$result['apptag'] = implode(',',$tags);
	    return $result;
	}
	
	//修改软件信息
	public function editapp($data)
    {	
		$validate = new ValidateApp();
		if(!$validate->scene('addapp')->check($data)){
		    return $validate->getError();
		}		
        $data['appuserid'] = db('user')->where('username',$data['appuser'])->value('id');
        $tagArr = array_unique(explode(";",$data['apptag']));
        $tagIds = [];
        foreach ($tagArr as $tag) {
            $id = db('apptag')->where('tag', $tag)->value('id');
            if ($id) {
                $tagIds[] = strval($id);
            } else {
                $tagIds[] = strval(db('apptag')->insertGetId(['tag' => $tag]));
            }
        }		
        $data['apptagid'] = implode(',', array_map('strval', $tagIds));	
        $id = $data["id"];
        unset($data["id"]);
        $result = $this->allowField(true)->save($data,["id"=>$id]);
        if($result){
            return 1;
        }else{
            return "服务器错误";
        }
    }
}