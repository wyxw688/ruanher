<?php

namespace app\admin\model;

use think\Model;

class Appupdate extends Model
{
    public function add($data)
    {
        $results = $this->where("versioncode",$data["versioncode"])->find();
        if($results){
            $versioncode = $data["versioncode"];
            unset($data["versioncode"]);
            $data["time"] = date("Y-m-d H:i:s",time());
            $result = $this->allowField(true)->save($data,["versioncode"=>$versioncode]);
        }else{
            $data["time"] = date("Y-m-d H:i:s",time());
            $result = $this->allowField(true)->save($data);
        }
		
		if ($result > 0) {
		    return 1;
		} else {
		    return "服务器错误";
		}
    }

    public function getnewappupdate()
    {
        $result = $this->order("time","desc")->field('versioncode,content,download,time,silentup,forceup,wifiup')->find();
        if(!$result){
            $result["versioncode"] = "1.0";
            $result["content"] = "";
            $result["download"] = "";
            $result["time"] = "";
        }
        return $result;
    }
}