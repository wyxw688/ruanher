<?php

namespace app\admin\model;

use app\admin\validate\Webset as DateWebset;
use think\Model;

class Webset extends Model
{
	//获取网站配置
	public function getwebset($type)
	{
	    $result = $this->find();
	    $value = json_decode($result,true);
	    return $value;
	}
	//编辑网站配置
	public function edit($data)
	{
	    $validate = new DateWebset();
	    if (!$validate->scene('editweb')->check($data)) {
	        return $validate->getError();
	    }
	    $result = $this->allowField(true)->save($data, ["webid" => 1]);
	    return 1;
	}
}