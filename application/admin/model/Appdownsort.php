<?php

namespace app\admin\model;

use app\admin\validate\Appdown as ValidateApp;
use think\Model;

class Appdownsort extends Model
{
    //获取所有软件分类
    public function getallappsort($data)
    {
        $result = db('appdownsort')
            ->order($data['sort'], $data['sortOrder'])
            ->limit($data["limit"])->page($data["page"])			
            ->select();
    	$count = db('appdownsort')
    	->count();
        return json(["rows" => $result, "total" => $count]);
    } 
	
	//新增软件分类数据
    public function addsort($data)
    {
        $validate = new ValidateApp();
        if(!$validate->scene('addsort')->check($data)){
            return $validate->getError();
        }
        $sortnamers = $this->where("sortname",$data["sortname"])->find();
        if($sortnamers){
            return "已存在该软件分类";
        }
        $result = $this->allowField(true)->save($data);
        if($result > 0){
            return 1;
        }else{
            return "服务器错误";
        }
    }
	
	//删除软件分类数据
	public function deletesort($id)
	{
	    if(!is_array($id)){
	        return "服务器错误";
	    }
	    foreach($id as $key=>$value){
	        $this->where("id",$value)->delete();
			$sortid=[
				"appsortid" => 1
			];
			db('appdown')->where("appsortid",$value)->update($sortid);
	    }
	    return 1;
	}
	
	//获取软件分类信息
	public function getappsortinfo($id)
	{
	    $result = $this->where("id",$id)->find();
	    return $result;
	}
	
	//修改软件信息
	public function editappsort($data)
	{	
		$validate = new ValidateApp();
		if(!$validate->scene('addsort')->check($data)){
		    return $validate->getError();
		}
		$id = $data["id"];
		unset($data["id"]);
	    $result = $this->allowField(true)->save($data,["id"=>$id]);
	    if($result){
	        return 1;
	    }else{
	        return "服务器错误";
	    }
	}
}