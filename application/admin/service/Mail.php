<?php

//定义邮件发送类
namespace app\admin\service;

use app\admin\model\App;
use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use think\Controller;
use think\db\Where;

class Mail
{

    /**
     * 邮箱发送
     *
     * @param [type] $setFrom  发件人名称
     * @param [type] $addAddress 收件人邮箱
     * @param [type] $Subject  邮件标题
     * @param [type] $Body  邮件内容
     * @return void
     */
    public static function sendEmail($setFrom, $addAddress, $Subject, $Body,$return = 1)
    {
        $model = new App();
        $emailinfo = $model->getapp(1);
        $setFrom = empty($setFrom) ? $emailinfo["emailname"] : $setFrom;
        $Subject = empty($Subject) ? $emailinfo["emailname"] : $Subject;
        $mail = new PHPMailer(true);
        try {
            //服务器配置
            $mail->CharSet = "UTF-8";                     //设定邮件编码
            $mail->SMTPDebug = 0;                        // 调试模式输出
            $mail->isSMTP();                             // 使用SMTP
            $mail->Host = $emailinfo["emailsite"];                // SMTP服务器
            $mail->SMTPAuth = true;                      // 允许 SMTP 认证
            $mail->Username = $emailinfo["email"];                // SMTP 用户名  即邮箱的用户名
            $mail->Password = $emailinfo["emailpass"];             // SMTP 密码  部分邮箱是授权码(例如163邮箱)
            $mail->SMTPSecure = 'ssl';                    // 允许 TLS 或者ssl协议
            $mail->Port = $emailinfo["emailport"];                            // 服务器端口 25 或者465 具体要看邮箱服务器支持

            $mail->setFrom($emailinfo["email"], $setFrom);  //发件人
            $mail->addAddress($addAddress);  // 收件人
            $mail->addReplyTo($emailinfo["email"], $setFrom); //回复的时候回复给哪个邮箱 建议和发件人一致
            //$mail->addCC('cc@example.com');                    //抄送
            //$mail->addBCC('bcc@example.com');                    //密送

            //发送附件
            // $mail->addAttachment('../xy.zip');         // 添加附件
            // $mail->addAttachment('../thumb-1.jpg', 'new.jpg');    // 发送附件并且重命名

            //Content
            $mail->isHTML(true);
            $mail->isHTML(true);                                  // 是否以HTML文档格式发送  发送后客户端可直接显示对应HTML内容
            $mail->Subject = $Subject;
            $mail->Body    = $Body;
            // $mail->AltBody = '如果邮件客户端不支持HTML则显示此内容';
            $mail->send();
            if($return == 1){
                return return_json(200,"邮件发送成功");
            }
            if($return == 2){
                return 1;
            }
        } catch (Exception $e) {
            if($return == 1){
                return return_json(400, "邮件发送失败：" . $mail->ErrorInfo);
            }
            if($return == 2){
                return "邮件发送失败：" . $mail->ErrorInfo;
            }
        }
    }
}
