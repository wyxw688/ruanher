<?php

/**
 * 封装一个上传图片的类
 */

namespace app\admin\service;

use Exception;
use think\facade\Request;

class Upload
{
    //上传配置
    private $config = [
        'image' => [
            "validate" => [
                'size' => 10 * 1024 * 1024,
                'ext' => ',jpg,png,gif,jpeg',
            ],
            "path" => 'uploads/images/',  //保存的路径
        ],
		'apk' => [
		    "validate" => [
		        'size' => 10 * 1024 * 1024,
		        'ext' => ',apk',
		    ],
		    "path" => 'uploads/apks/',  //保存的路径
		],
        'attachment' => [
            "validate" => [
                'size' => 100 * 1024 * 1024,
                'ext' => ',zip',
            ],
            "path" => 'uploads/attachment/',  //保存的路径
        ],
    ];

    private $domain;

    public function __construct()
    {
        $this->domain = Request::domain();
    }

    public function upload($filename, $retuen = 0)
    {
        if (empty($_FILES) || empty($_FILES[$filename])) {
            return [];
        }
        try {
            $file = request()->file($filename);
            if (is_array($file)) {
                $path = [];
                foreach ($file as $item) {
                    $detail = $item->getInfo();
                    $returnData['name'] = $detail['name'];
                    $returnData['type'] = $detail['type'];
                    $returnData['size'] = $detail['size'];
                    $returnData['filePath'] = $this->save($item);
                    $returnData['fullPath'] = $this->domain . $returnData['filePath'];
                    $path[] = $returnData;
                }
            } else {
                $detail = $file->getInfo();
                $returnData['name'] = $detail['name'];
                $returnData['type'] = $detail['type'];
                $returnData['size'] = $detail['size'];
                $returnData['filePath'] = $this->save($file);
                $returnData['fullPath'] = $this->domain . $returnData['filePath'];
                $path = $returnData;
            }
            if ($retuen == 0) {
                return return_json(200, "上传成功", $path);
            }
            if ($retuen == 1) {
                return json_encode(array("code" => 200, "data" => $path));
            }
        } catch (\Exception $e) {
            if ($retuen == 0) {
                return return_json(400, $e->getMessage());
            }
            if ($retuen == 1) {
                return json_encode(array("code" => 400, "msg" => $e->getMessage()));
            }
        }
    }


    public function save($file)
    {
        $config = $this->getConfig($file->getInfo());
        if (empty($config)) {
            return return_json(400, "不允许上传该文件类型");
        }
        //移动到相对应的目录文件夹下
        $result = $file->move($config['path']);
        if ($result) {
            $path = $config['path'];
            $getsaveName = str_replace("\\", "/", $result->getSaveName());
            return "/" . $path . $getsaveName;
        } else {
            return return_json(400, $file->getError());
        }
    }

    private function getConfig($file)
    {
        $name = pathinfo($file["name"]);
        $ext = $name["extension"];
        //这样遍历为了以后增加其他的文件类型
        foreach ($this->config as $key => $value) {
            if ((string)strpos($value["validate"]["ext"], $ext) != false) {
                return $this->config[$key];
            }
        }
        return null;
    }
}
