<?php


namespace app\admin\service;

use phpqrcode\QRcode;

class QrCodeService
{
    public static function lineQrCode($urlstr, $out_trade_no)
    {
        $errorCorrectionLevel = 'L';    //容错级别
        $matrixPointSize = 5;            //生成图片大小
        $path = 'uploads/pay/' . $out_trade_no .".png";
        QRcode::png($urlstr, $path, $errorCorrectionLevel, $matrixPointSize, 2);
    }
}
