<?php

namespace app\admin\validate;

use think\Validate;

class Appdown extends Validate
{
    protected $rule =   [
        'sortname|软件分类名称'  => 'require',
        'sorticon|分类图标地址'   => 'require',
		'appname'=> 'require',
		'appdownurl'=> 'require',
		'apppage'=> 'require',
		'appbb'=> 'require',
		'appdata'=> 'require',
		'appicon'=> 'require',
		'apphenfu'=> 'require',
		'appsortid'=> 'number|require',
		'apptag'=> 'require',
		'appsce'=> 'require',
		'appmod'=> 'number|require',
		'appisvip'=> 'number|require',
		'apptop'=> 'number|require',
		'appuser'=> 'require'
    ];

    //验证场景
    protected $scene = [
        'addsort'  =>  ['sortname','sorticon'],
		'addapp' => ['appname','appdownurl','apppage','appbb','appdata','appicon','apphenfu','appsortid','apptag','appsce','appmod','appisvip','apptop','appuser']
    ];



}