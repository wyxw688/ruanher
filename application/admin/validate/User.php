<?php

namespace app\admin\validate;

use think\Validate;

class User extends Validate
{
    protected $rule =   [
        'username|用户名'  => 'require|alphaNum',
        'password|密码'   => 'require|min:6|alphaNum',
        'usertx|用户头像'   => 'require',
        'nickname|昵称'   => 'require',
        'money|积分' => 'require|number',
        'viptime|vip时间' => 'require|number',
        'sex|性别' => 'require',
        'useremail|邮箱' => 'require|email',
        'signature|个性签名' => 'require',
    ];

    //验证场景
    protected $scene = [
        'add'  =>  ['username','password','email'],
        'edit'  =>  ['usertx', 'nickname', 'money', 'viptime', 'sex', 'useremail'],
    ];
}