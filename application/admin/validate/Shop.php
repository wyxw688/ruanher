<?php

namespace app\admin\validate;

use think\Validate;

class Shop extends Validate
{
    protected $rule =   [
        'name|会员名称'  => 'require',
        'detail|会员描述' => 'require',
        'price|会员价格' => 'require',
        'vipdate|会员到期时间' => 'require',
    ];

    //验证场景
    protected $scene = [
        'add' => ['name','detail','price','vipdate'],
    ];



}