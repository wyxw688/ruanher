<?php

namespace app\admin\validate;

use think\Validate;

class Notes extends Validate
{
    protected $rule =   [
        'title|标题'  => 'require',
        'content|内容'   => 'require',
    ];

    //验证场景
    protected $scene = [
        'edit'  =>  ['title','content'],
    ];



}