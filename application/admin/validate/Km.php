<?php

namespace app\admin\validate;

use think\Validate;

class Km extends Validate
{
    protected $rule =   [
        'km|卡密'  => 'require',
        'money|金币' => 'require|number',
        'viptime|会员天数' => 'require|number',
        'num|数量' => 'require|number',
        'length|长度' => 'require|number',
        'expire|过期时间' => 'require|number',
    ];

    //验证场景
    protected $scene = [
        'add' => ['money','viptime','num','length']
    ];



}