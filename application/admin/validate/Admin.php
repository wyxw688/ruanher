<?php

namespace app\admin\validate;

use think\Validate;

class Admin extends Validate
{
    protected $rule =   [
        'adminname|账号'  => 'require|max:25',
        'adminpass|密码'   => 'require|min:6|alphaNum',
        'newpassword|新密码'   => 'require|min:6|alphaNum',
        'confirmpassword|确认新密码'   => 'require|min:6|confirm:newpassword',
        'adminqq|QQ' => 'require|number',
        'nickname|昵称' => 'require',
        'captcha|验证码' => 'require|captcha',
    ];

    //验证场景
    protected $scene = [
        'login'  =>  ['adminname','adminpass','captcha'],
        'edit'  =>  ['adminname','adminqq','nickname'],
        'editpass'  =>  ['adminpass','newpassword','confirmpassword'],
    ];



}