<?php

namespace app\admin\validate;

use think\Validate;

class Webset extends Validate
{
    protected $rule =   [
        'rhtitle|网站标题'  => 'require',
        'rhkeywords|网站关键词'   => 'require',
        'rhdescription|网站简介'   => 'require',
        'rhlogo|网站深色LOGO'   => 'require',
        'rhlogo1|网站浅色LOGO' => 'require',
        'rhindeximg|APP推广图' => 'require',
        'rhappimg1|APP截图1' => 'require',
        'rhappimg2|APP截图2' => 'require',
        'rhappimg3|APP截图3' => 'require',
        'rhappimg4|APP截图4' => 'require',
        'rhappimg5|APP截图5' => 'require',
		'rhappimg6|APP截图6'  => 'require',
		'rhappimg7|APP截图7'   => 'require',
		'rhappimg8|APP截图8'   => 'require',
		'rhtabimg1|APP首页截图1'   => 'require',
		'rhtabimg2|APP首页截图2' => 'require',
		'rhtabimg3|APP首页截图3' => 'require',
		'rhmsg|关于我们' => 'require',
		'rhappicon|软件图标' => 'require',
		'rhbeian|网站备案号' => 'require',
		'rhqq|加客服QQ链接' => 'require',
		'rhqun|加官方Q群链接' => 'require',
    ];

    //验证场景
    protected $scene = [
        'editweb'  =>  ['rhtitle', 'rhkeywords', 'rhdescription', 'rhlogo', 'rhlogo1', 'rhindeximg', 'rhtabimg1', 'rhtabimg2', 'rhtabimg3', 'rhappimg1', 'rhappimg2', 'rhappimg3', 'rhappimg4', 'rhappimg5', 'rhappimg6', 'rhappimg7', 'rhappimg8',
		 'rhmsg', 'rhappicon', 'rhbeian', 'rhqq', 'rhqun'],
    ];
}