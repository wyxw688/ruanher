<?php

namespace app\admin\validate;

use think\Validate;

class App extends Validate
{
    protected $rule =   [
        'rhqq|客服QQ'   => 'require|number',
        'rhqun|官方群key' => 'require',
        'zc_money|注册赠送积分' => 'require|number',
        'zc_vip|注册赠送会员' => 'require|number',
        'sign_money|签到赠送金币' => 'require|number',
        'sign_vip|签到赠送会员' => 'require|number',
        'invitation_money|邀请人获得金币' => 'require|number',
        'invitation_vip|邀请人获得会员' => 'require|number',
        'appmsg|公告内容' => 'require',
		'email|邮箱' => 'require|email',
		'emailpass|邮箱秘钥' => 'require',
		'emailport|发件端口' => 'require|number',
		'emailsite|SMTP地址' => 'require',
		'emailname|发件人' => 'require',
		'signiv|接口秘钥偏移值' => 'require|number',
		'signkey|接口秘钥' => 'require|number',
		'payapi|支付接口' => 'require',
		'payid|商户ID' => 'require',
		'paykey|商户秘钥' => 'require',
		'aliid|支付宝ID' => 'require',
		'aliprivatekey|支付宝私钥' => 'require',
		'alikey|支付宝公钥' => 'require',
		'paynotify|异步通知地址' => 'require',
		'appmsg|公告内容' => 'require',
		'versioncode|版本号' => 'require',
		'content|更新内容' => 'require',
		'download|下载地址' => 'require',
		'adname|广告标题' => 'require',
		'adinmurl|广告横幅' => 'require',
		'adlink|广告链接' => 'require',
		'adtype|显示位置' => 'require',
	];
    //验证场景
    protected $scene = [
        'editzuce' => ['zc_money','zc_vip','sign_money','sign_vip','invitation_money','invitation_vip'],
		'editemail' => ['email','emailpass','emailport','emailsite','emailname'],
		'editappus' => ['rhqq','rhqun','signiv','signkey'],
		'editapppay' => ['payapi','payid','paykey','aliid','aliprivatekey','alikey','paynotify'],
		'editappmsg' => ['appmsg'],
		'editad' => ['adname','adimgurl','adlink','adtype'],
		'addad' => ['adname','adimgurl','adlink','adtype'],
	];
}