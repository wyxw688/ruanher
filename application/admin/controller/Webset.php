<?php

namespace app\admin\controller;

use think\Db; 

class Webset extends Base
{	
	//进入网站配置、保存配置
    public function webset()
    {	
		if (request()->isAjax()) {
		    $data = [
		        "rhtitle" => input("post.rhtitle"),
		        "rhkeywords" => input("post.rhkeywords"),
		        "rhdescription" => input("post.rhdescription"),
				"rhlogo" => input("post.rhlogo"),
				"rhlogo1" => input("post.rhlogo1"),
				"rhindeximg" => input("post.rhindeximg"),
				"rhappimg1" => input("post.rhappimg1"),
				"rhappimg2" => input("post.rhappimg2"),
				"rhappimg3" => input("post.rhappimg3"),
				"rhappimg4" => input("post.rhappimg4"),
				"rhappimg5" => input("post.rhappimg5"),
				"rhappimg6" => input("post.rhappimg6"),
				"rhappimg7" => input("post.rhappimg7"),
				"rhappimg8" => input("post.rhappimg8"),
				"rhmsg" => input("post.rhmsg"),
				"rhappicon" => input("post.rhappicon"),
				"rhbeian" => input("post.rhbeian"),
				"rhqq" => input("post.rhqq"),
				"rhqun" => input("post.rhqun"),
				"rhtabimg1" => input("post.rhtabimg1"),
				"rhtabimg2" => input("post.rhtabimg2"),
				"rhtabimg3" => input("post.rhtabimg3"),
		    ];
		    $result = model('webset')->edit($data);
		    if ($result == 1) {
		        $this->success("修改成功");
		    } else {
		        $this->error($result);
		    }
		} else {
		    $result = model("webset")->getwebset("webset");
		    $this->assign("webset", $result);
		    return $this->fetch();
		}        
    }
	
	
}