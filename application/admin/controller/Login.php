<?php

namespace app\admin\controller;

use think\Controller;

class Login extends Controller
{
    //登录
    public function index()
    {
        //防止重复登陆
        if (session("?admin")) {
            $this->redirect('admin/index/index');
        }
        //判断是否选择了5天内免登录
        if (cookie("remembermedata.rememberme") == "1") {
            $this->redirect('admin/index/index');
        }
        if (request()->isAjax()) {
            $data = [
                'adminname' => input("post.adminname"),
                'adminpass' => input("post.adminpass"),
                'captcha' => input("post.captcha"),
                'rememberme' => input("post.rememberme"),
            ];
            $result = model("admin")->login($data);
            if ($result == 1) {
                $this->success("登录成功", "admin/index/index");
            } else {
                $this->error($result);
            }
        } else {
            return $this->fetch();
        }
    }
}
