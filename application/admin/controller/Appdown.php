<?php

namespace app\admin\controller;

use think\Db;
use think\facade\Request;

class Appdown extends Base
{
	//进入软件管理
    public function index()
    {
        return $this->fetch();
    }

    //获取所有软件
    public function getallapp()
    {
        $data = [
            "sort" => input('?sort') ? ''.input('sort') : 'id',
            "sortOrder" => input("?sortOrder") ? input("sortOrder") : "desc",
            "appname" => input("appname"),
            "limit" => input('limit')?input('limit'):10,
            "page" => input('page')?input('page'):1,
        ];
        $result = model("appdown")->getallapp($data);
        return $result;
    }
	
	//删除软件
	public function deleteapp()
	{
	    $id = explode(",", input('id'));
	    $result = model("appdown")->deleteapp($id);
	    if ($result == 1) {
	        return $this->success("删除成功");
	    } else {
	        return $this->error($result);
	    }
	}
	
	//审核软件
	public function stateapp()
	{
	    $id = explode(",", input('id'));
	    $result = model("appdown")->editstate($id);
	    if ($result == 1) {
	        return $this->success("修改成功");
	    } else {
	        return $this->error($result);
		}
	}
	
	//一键审核软件
	public function stateallapp(){
	    $result = model("appdown")->editallstate();
	    if ($result == 1) {
	        return $this->success("审核成功");
	    } else {
	        return $this->error($result);
	    }
	}
	
	//新增软件
	public function addapp(){
        $data = input("post.");
        $result = model("appdown")->addapp($data);
        if($result == 1){
            return $this->success("添加成功");
        }else{
            return $this->error($result);
        }
    }
	
	//编辑软件
	public function editapp()
	{	
		if (request()->isAjax()) {
			$data = input();
		    $result = model("appdown")->editapp($data);
		    if ($result == 1) {
		        return $this->success("修改成功");
		    } else {
		        return $this->error($result);
		    }
		} else {
		    $id = input("id");
		    if ($id == "") {
		        $this->error("服务器错误");
		    }
		    $result = model("appdown")->getappinfo($id);
		    if ($result == null) {
		        $this->error("服务器错误");
		    }
		    $this->assign("appdown", $result);
		    return $this->fetch('appedit');
		}	    
	}
	
	
	//进入分类管理
	public function appdownsort()
	{
	    return $this->fetch();
	}
	
	//获取所有软件分类
	public function getallappsort()
	{
	    $data = [
	        "sort" => input('?sort') ? ''.input('sort') : 'id',
	        "sortOrder" => input("?sortOrder") ? input("sortOrder") : "asc",
	        "limit" => input('limit')?input('limit'):10,
	        "page" => input('page')?input('page'):1,
	    ];
	    $result = model("appdownsort")->getallappsort($data);
	    return $result;
	}
	
	//新增软件分类
	public function addsort()
	{
	    $data = input("post.");
	    $result = model("appdownsort")->addsort($data);
	    if($result == 1){
	        return $this->success("添加成功");
	    }else{
	        return $this->error($result);
	    }
	}
	
	//删除软件分类
	public function deletesort()
	{
	    $id = explode(",", input('id'));
	    $result = model("appdownsort")->deletesort($id);
	    if ($result == 1) {
	        return $this->success("删除成功");
	    } else {
	        return $this->error($result);
	    }
	}
	
	//编辑软件分类
	public function editappsort()
	{	
		if (request()->isAjax()) {
			$data = input();
		    $result = model("appdownsort")->editappsort($data);
		    if ($result == 1) {
		        return $this->success("修改成功");
		    } else {
		        return $this->error($result);
		    }
		} else {
		    $id = input("id");
		    if ($id == "") {
		        $this->error("服务器错误");
		    }
		    $result = model("appdownsort")->getappsortinfo($id);
		    if ($result == null) {
		        $this->error("服务器错误");
		    }
		    $this->assign("appdownsort", $result);
		    return $this->fetch('appsortedit');
		}	    
	}

	public function upapk()
	{
		return $this->fetch();
	}
}