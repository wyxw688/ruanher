<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;

class Mupdate extends Controller
{
    //使用curl访问
    function curl_request($url)
    {
        $headerArray = array("Content-type:application/json;", "Accept:application/json");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public function getupdate()
    {
        $url = "https://www.moranblog.cn/ht.php";
        $res = $this->curl_request($url);
        $res = json_decode($res, true);
        return $this->success('检测成功', '', $res);
    }

    public function downloadzip()
    {
        $remoteFileUrl = input("url");
        //本地存储路径绝对地址
        $saveFilePath = "./update/morangxb.zip";
        $curlHandler = curl_init();
        curl_setopt($curlHandler, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curlHandler, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt_array($curlHandler, [
            CURLOPT_URL => $remoteFileUrl,
            CURLOPT_FILE => fopen($saveFilePath, 'w+')
        ]);
        curl_exec($curlHandler);
        if (curl_errno($curlHandler) === CURLE_OK) {
            return return_json(200, "下载成功", $saveFilePath);
        }
        curl_close($curlHandler);
        return return_json(400, "更新失败，请联系作者");
    }

    //解压文件 覆盖原文件
    public function unzip()
    {
        $file_path = "./update/morangxb.zip";
        $zip = new \ZipArchive();
        $res = $zip->open($file_path);  //解压文件
        //解压目录
        try {
            if ($res === true) {
                $zip->extractTo('../');
                $zip->close();
                unlink($file_path); //删除源文件
                return return_json(200, '解压成功');
            } else {
                return return_json(400, '解压失败');
            }
        } catch (\Exception $e) {
            return return_json(400, '请设置文件权限为755(www)');
        }
    }

    //执行sql文件
    public function runsql()
    {
        $issql = File_exists('../sql.sql');
        if ($issql) {
            $sql = file_get_contents('../sql.sql');
            $sql = array_reverse(explode(';', $sql));
            try {
                foreach ($sql as $key => $value) {
                    Db::execute($value);
                }
                unlink('../sql.sql');
                return return_json(200,'执行成功');
            } catch (\Exception $e) {
                return return_json(200,'执行成功');
            }
        } else {
            return return_json(200,'文件不存在');
        }
    }

    public function mrhtuserlogin()
    {
        $data = input();
        $url = "http://user.moranblog.cn/login/".$data["username"]."/".$data["password"];
        $res = $this->curl_request($url);
        $res = json_decode($res, true);
        if($res != null){
            if(isset($res["code"]) && $res["code"] == 200){
                $file_path = fopen("moranuser.txt", "w") or die("Unable to open file!");
                $txt = $data["username"].",".$data["password"];
                fwrite($file_path, $txt);
                return $this->success('登录成功', '', $res);
            }
            if(isset($res["code"]) && $res["code"] == 400){
                return $this->error('密码错误', '', $res);
            }
        }
        return $this->error('服务器错误');
    }
}
