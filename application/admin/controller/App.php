<?php

namespace app\admin\controller;

use app\admin\service\Mail;
use app\admin\service\MailSendingTemplate;

class App extends Base
{
	//进入登录注册页面
    public function index()
    {        
		if (request()->isAjax()) {
		    $data = [
		        "zc_money" => input("post.zc_money"),
		        "zc_vip" => input("post.zc_vip"),
		        "sign_money" => input("post.sign_money"),
				"sign_vip" => input("post.sign_vip"),
				"invitation_money" => input("post.invitation_money"),
				"invitation_vip" => input("post.invitation_vip")
		    ];
		    $result = model('app')->editzuce($data);
		    if ($result == 1) {
		        $this->success("修改成功");
		    } else {
		        $this->error($result);
		    }
		} else {
		    $result = model("app")->getapp("app");
		    $this->assign("app", $result);
		    return $this->fetch();
		}
    }
	//进入邮件管理
	public function email()
	{	
		if (request()->isAjax()) {
		    $data = [
		        "email" => input("post.email"),
		        "emailpass" => input("post.emailpass"),
		        "emailport" => input("post.emailport"),
				"emailsite" => input("post.emailsite"),
				"emailname" => input("post.emailname")				
		    ];
		    $result = model('app')->editemail($data);
		    if ($result == 1) {
		        $this->success("修改成功");
		    } else {
		        $this->error($result);
		    }
		} else {
		    $result = model("app")->getapp("app");
		    $this->assign("email", $result);
		    return $this->fetch();
		}	    
	}
	//测试发送邮件
	public function TestEmail()
	{
	    $addAddress = input('test_email');
	    return Mail::sendEmail('',$addAddress,"",MailSendingTemplate::testemail());
	}
	//进入版本发布页面
	public function appupdate()
	{	
		return $this->fetch();
	}
	//获取版本列表
	public function getappupdate()
	{
		$data = [
		    "sort" => input('?sort') ? input('sort') : 'id',
		    "sortOrder" => input("?sortOrder") ? input("sortOrder") : "asc",
		    "versioncode" => input("versioncode"),
		    "limit" => input('limit')?input('limit'):10,
		    "page" => input('page')?input('page'):1,
		];
		$result = model("app")->getappupdate($data);
		return $result;
	}
	//发布新版本
	public function addappupdate()
	{
	    $data = input();
	    $result = model("appupdate")->add($data);
	    if($result == 1){
	        return $this->success("发布成功");
	    }else{
	        return $this->error($result);
	    }
	}
	//进入软件信息界面
	public function appus()
	{	
		if (request()->isAjax()) {
		    $data = [
		        "rhqq" => input("post.rhqq"),
		        "rhqun" => input("post.rhqun"),
		        "signkey" => input("post.signkey"),
				"signiv" => input("post.signiv")			
		    ];
		    $result = model('app')->editappus($data);
		    if ($result == 1) {
		        $this->success("修改成功");
		    } else {
		        $this->error($result);
		    }
		} else {
		    $result = model("app")->getapp("app");
		    $this->assign("appus", $result);
		    return $this->fetch();
		}
	}
	//进入支付配置页面
	public function apppay()
	{	
		if (request()->isAjax()) {
		    $data = [
		        "payapi" => input("post.payapi"),
		        "payid" => input("post.payid"),
		        "paykey" => input("post.paykey"),
				"aliid" => input("post.aliid"),
				"aliprivatekey" => input("post.aliprivatekey"),
				"alikey" => input("post.alikey"),
				"paynotify" => input("post.paynotify")
		    ];
		    $result = model('app')->editapppay($data);
		    if ($result == 1) {
		        $this->success("修改成功");
		    } else {
		        $this->error($result);
		    }
		} else {
		    $result = model("app")->getapp("app");
		    $this->assign("apppay", $result);
		    return $this->fetch();
		}	    
	}
	//进入公告发布页面
	public function appmsg()
	{	
		if (request()->isAjax()) {
		    $data = [
		        "appmsg" => input("post.appmsg")
		    ];
		    $result = model('app')->editappmsg($data);
		    if ($result == 1) {
		        $this->success("修改成功");
		    } else {
		        $this->error($result);
		    }
		} else {
		    return $this->fetch();
		}	    
	}
	//获取公告列表
	public function getappmsg()
	{
		$data = [
		    "sort" => input('?sort') ? input('sort') : 'id',
		    "sortOrder" => input("?sortOrder") ? input("sortOrder") : "asc",
		    "appmsg" => input("appmsg"),
		    "limit" => input('limit')?input('limit'):10,
		    "page" => input('page')?input('page'):1,
		];
		$result = model("app")->getappmsg($data);
		return $result;
	}

	//删除版本
	public function deleteup()
    {
        $id = explode(",", input('id'));
        $result = model("app")->deleteup($id);
        if ($result == 1) {
            return $this->success("删除成功");
        } else {
            return $this->error($result);
        }
    }

	//进入广告管理
    public function appad()
    {
        return $this->fetch();
    }

	//获取所有广告
    public function getallad()
    {
        $data = [
            "sort" => input('?sort') ? ''.input('sort') : 'id',
            "sortOrder" => input("?sortOrder") ? input("sortOrder") : "desc",
            "appname" => input("appname"),
            "limit" => input('limit')?input('limit'):10,
            "page" => input('page')?input('page'):1,
        ];
        $result = model("appad")->getallad($data);
        return $result;
    }

	//新增广告
	public function addad()
	{
	    $data = input("post.");
	    $result = model("appad")->addad($data);
	    if($result == 1){
	        return $this->success("添加成功");
	    }else{
	        return $this->error($result);
	    }
	}
	
	//删除广告
	public function deletead()
	{
	    $id = explode(",", input('id'));
	    $result = model("appad")->deletead($id);
	    if ($result == 1) {
	        return $this->success("删除成功");
	    } else {
	        return $this->error($result);
	    }
	}
	
	//编辑广告
	public function editad()
	{	
		if (request()->isAjax()) {
			$data = input();
		    $result = model("appad")->editad($data);
		    if ($result == 1) {
		        return $this->success("修改成功");
		    } else {
		        return $this->error($result);
		    }
		} else {
		    $id = input("id");
		    if ($id == "") {
		        $this->error("服务器错误");
		    }
		    $result = model("appad")->getadinfo($id);
		    if ($result == null) {
		        $this->error("服务器错误");
		    }
		    $this->assign("appad", $result);
		    return $this->fetch('appadedit');
		}	    
	}

}
