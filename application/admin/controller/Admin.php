<?php

namespace app\admin\controller;

use think\Db;

class Admin extends Base
{
    //管理员信息
    public function admininfo()
    {
        if (request()->isAjax()) {
            $data = [
                "adminname" => input("post.adminname"),
                "nickname" => input("post.nickname"),
                "adminqq" => input("post.adminqq"),
            ];
            $result = model('admin')->edit($data);
            if ($result == 1) {
                $this->success("修改成功");
            } else {
                $this->error($result);
            }
        } else {
            $result = model('admin')->adminInfo();
            $this->assign("admininfo",$result);
            return $this->fetch();
        }
    }

    //修改新的key值
    public function generatekey()
    {
        $result = model("admin")->generatekey(1);
        $this->success($result);
    }

    public function adminlog()
    {
        $res = Db::name("system_log")->paginate(10);
        $this->assign("res",$res);
        return $this->fetch();
    }


    //关闭key
    public function closeKey()
    {
        $result = model("admin")->generatekey(2);
        $this->success("关闭成功");
    }

    //修改密码
    public function updatepassword()
    {
        if (request()->isAjax()) {
            $data = [
                "adminpass" => input("post.oldpassword"),
                "newpassword" => input("post.newpassword"),
                "confirmpassword" => input("post.confirmpassword"),
            ];
            $result = model('admin')->editpass($data);
            if ($result == 1) {
                session(null);
                cookie(null, 'think_');
                $this->success("修改成功");
            } else {
                $this->error($result);
            }
        } else {
            return $this->fetch();
        }
    }

    //退出登录
    public function loginout()
    {
        session(null);
        cookie(null, 'think_');
        $this->success("退出登录成功");
    }
}
