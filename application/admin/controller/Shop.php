<?php

namespace app\admin\controller;

class Shop extends Base
{
    public function index()
    {
        return $this->fetch();
    }

    public function getshoplist()
    {
        $data = [
            "sort" => input('?sort') ? input('sort') : 'id',
            "sortOrder" => input("?sortOrder") ? input("sortOrder") : "asc",
            "shopname" => input("shopname"),
            "limit" => input('limit')?input('limit'):10,
            "page" => input('page')?input('page'):1,
        ];
        $result = model("shop")->getshoplist($data);
        return $result;
    }

    public function add()
    {
        $data = input();
        $result = model("shop")->add($data);
        if($result == 1){
            return $this->success("添加成功");
        }else{
            return $this->error($result);
        }
    }

    public function edit()
    {
        if (request()->isAjax()) {
            $data = input();
            $result = model("shop")->edit($data);
            if ($result == 1) {
                return $this->success("修改成功");
            } else {
                return $this->error($result);
            }
        } else {
            $id = input("id");
            if ($id == "") {
                $this->error("服务器错误");
            }
            $result = model("shop")->getshopinfo($id);
            if ($result == null) {
                $this->error("服务器错误");
            }
            $this->assign("data", $result);
            return $this->fetch();
        }
    }

    public function delete()
    {
        $id = explode(",", input('id'));
        $result = model("shop")->deleteshop($id);
        if ($result == 1) {
            return $this->success("删除成功");
        } else {
            return $this->error($result);
        }
    }

    public function shoporder()
    {
        return $this->fetch();
    }

    public function getshoporderlist()
    {
        $data = [
            "sort" => input('?sort') ? input('sort') : 'id',
            "sortOrder" => input("?sortOrder") ? input("sortOrder") : "asc",
            "limit" => input('limit')?input('limit'):10,
            "page" => input('page')?input('page'):1,
        ];
        $result = model("shop")->getshoporderlist($data);
        return $result;
    }

    public function deleteorder()
    {
        $id = explode(",", input('id'));
        $result = model("shop")->deleteorder($id);
        if ($result == 1) {
            return $this->success("删除成功");
        } else {
            return $this->error($result);
        }
    }

    public function deleteorderall()
    {
        $result = model("shop")->deleteorderall();
        if ($result == 1) {
            return $this->success("删除成功");
        } else {
            return $this->error($result);
        }
    }


}