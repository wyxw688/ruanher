<?php

namespace app\admin\controller;

use app\admin\service\Pay;
use app\admin\service\Ypay;
use app\admin\service\Yipay;
use app\api\controller\Common;

class Block
{
    //支付宝的查询订单
    public function queryOrder()
    {
        $result = db("order")
            ->alias("o")
            ->join("shop s", "s.id=o.shop_id")
            ->field("o.*,s.payment,s.content,s.type")
            ->where("s.payment", 0)
            ->where("o.status", 0)
            ->select();
        foreach ($result as $key => $value) {
            if (time() - strtotime($value["createtime"]) < 10800) {
                $payment = Pay::createPaymentInstance($value["payment"]);
                $sentData = $payment->queryOrder(Pay::getPaymentById($value["payment"]), $value["order_no"]);
                if ($sentData["code"] == 200) {
                    //修改订单状态
                    $update = [
                        "status" => 1,
                        "transaction_no" => $sentData["data"]["trade_no"],
                        "paymenttime" => $sentData["data"]["send_pay_date"],
                    ];
                    db("order")->where("id", $value["id"])->update($update);
                    //增加会员天数
                    if ($value["type"] == 0) {
                        $userinfo = db("user")->where("id", $value["userid"])->find();
                        if ($userinfo["viptime"] < time()) {
                            $viptime = time() + 3600 * 24 * $value["content"];
                        } else {
                            $viptime = $userinfo["viptime"] + 3600 * 24 * $value["content"];
                        }
                        db("user")->where("id", $value["userid"])->update([
                            "viptime" => $viptime,
                        ]);
                    }
                }
            }
        }
        echo "查询成功";
    }

    public function yipay_server_back()
    {
        $callbackData = array_merge($_POST, $_GET);
        $sign = $callbackData["sign"];
        $paymentRow = db("payment")->where("payment_id", 4)->find();
        $Ypay = new Yipay();
        $result = $Ypay->getEpaySignVeryfy($callbackData, $sign, $paymentRow["private_key"]);
        if ($result) {
            //验签成功
            //修改订单状态
            $update = [
                "status" => 1,
                "transaction_no" => $callbackData["trade_no"],
                "paymenttime" => date("Y-m-d H:i:s", time()),
            ];
            $orderarr = db("order")->where("order_no", $callbackData["out_trade_no"])->find();
            if ($orderarr != null) {
                if ($orderarr["status"] == 0) {
                    if ($callbackData['trade_status'] == 'TRADE_SUCCESS') {
                        db("order")->where("order_no", $callbackData["out_trade_no"])->update($update);
                        //查询商品信息
                        $shoparr = db("shop")->where("id", $orderarr["shop_id"])->find();
                        if ($shoparr["type"] == 0) {
                            $userinfo = db("user")->where("id", $orderarr["userid"])->find();
                            if ($userinfo["viptime"] < time()) {
                                $viptime = time() + 3600 * 24 * $shoparr["content"];
                            } else {
                                $viptime = $userinfo["viptime"] + 3600 * 24 * $shoparr["content"];
                            }
                            db("user")->where("id", $userinfo["id"])->update([
                                "viptime" => $viptime,
                            ]);
                        }
                        $addmsg = new Common();
                        $addmsg->addSysMessage(4, $userinfo["id"], $shoparr["id"]);
                        return 'success';
                    }
                }
            }
        }
    }
}
