<?php

namespace app\admin\controller;

use app\admin\service\Upload;
use think\facade\Cache;
use think\helper\Time;

class Index extends Base
{
    public function index()
    {
        $data = array();
        list($starttime, $endtime) = Time::today();
        $data["usertotal"] = db("user")->count();
        $data["userviptotal"] = db("user")->where("viptime", ">", time())->count();
        $data["viewtotal"] = db("app")->find();
        $data["kmtotal"] = db("km")->count();
		$data["apptotal"] = db("appdown")->count();
        $data["signtotal"] = db("sign")->where("lasttime", ">", $starttime)->count();
        $getWeekRegister = $this->getWeekRegister();
        $data["registercount"] = json_encode($getWeekRegister[0]);
        $data["registertime"] = json_encode($getWeekRegister[1]);
        $getpaymoney = $this->getpaymoney();
        $data["paycount"] = json_encode($getpaymoney[0]);
        $data["paytime"] = json_encode($getpaymoney[1]);
        $this->assign("data", $data);
        return $this->fetch();
    }

    public function clear_all()
    {
        $pathArr = [
            'LOG_PATH'   => env('runtime_path') . 'log/',
            'CACHE_PATH' => env('runtime_path') . 'cache/',
            'TEMP_PATH'  => env('runtime_path') . 'temp/'
        ];
        $dirs = (array) glob($pathArr['LOG_PATH'] . '*');
        foreach ($dirs as $dir) {
            array_map('unlink', glob($dir . '/*.log'));
        }
        array_map('rmdir', $dirs);
        array_map('unlink', glob($pathArr['TEMP_PATH'] . '/*.*'));
        Cache::clear();
        $this->success("清除成功");
    }

    //后台统一请求上传类
    public function upload()
    {
        $upload = new Upload();
        $result = $upload->upload('file');
        return $result;
    }

    //统计最近7天的注册量
    public function getWeekRegister()
    {
        $resultcount = [];
        $resulttime = [];
        list($totaystatrttime, $totayendtime) = Time::today();
        for ($i = 6; $i >= 0; $i--) {
            $date = date('Y-m-d', strtotime("-" . $i . "day"));
            $start = date('Y-m-d H:i:s', strtotime("-" . $i . "day", $totaystatrttime));
            $end = date('Y-m-d H:i:s', strtotime("-" . $i . "day", $totayendtime));
            $count = db("user")->where('create_time', 'between time', [$start, $end])->count();
            array_push($resultcount, $count);
            array_push($resulttime, $date);
        }
        return [$resultcount, $resulttime];
    }

    //支付金额统计
    public function getpaymoney()
    {
        $resultcount = [];
        $resulttime = [];
        list($totaystatrttime, $totayendtime) = Time::today();
        for ($i = 6; $i >= 0; $i--) {
            $date = date('Y-m-d', strtotime("-" . $i . "day"));
            $start = date('Y-m-d H:i:s', strtotime("-" . $i . "day", $totaystatrttime));
            $end = date('Y-m-d H:i:s', strtotime("-" . $i . "day", $totayendtime));
            $count = db("order")
                ->alias("o")
                ->join("shop s", "s.id=o.shop_id")
                ->field("o.*,s.price")
                ->where("o.status", 1)
                ->where('o.createtime', 'between time', [$start, $end])
                ->sum("o.price");
            array_push($resultcount, $count);
            array_push($resulttime, $date);
        }
        return [$resultcount, $resulttime];
    }

    //验证是否登录
    public function moranuser()
    {
        $path = '../public/moranuser.txt'; //文件的存放路径
        if (file_exists($path)) { //首先判断文件存在不存在
            $file = file_get_contents('moranuser.txt');
            $rep = str_replace("\r\n", ',', $file);
            $userpass = explode(',', $rep);
            $url = new Mupdate();
            $username = isset($userpass[0]) ? $userpass[0] : '';
            $password = isset($userpass[1]) ? $userpass[1] : '';
            if($username == '' || $password == "") {
                return return_json(400, "未登录");
            }
            $sendurl = "http://user.moranblog.cn/login/".$username."/".$password;
            $res = $url->curl_request($sendurl);
            $res = json_decode($res, true);
            if(isset($res["code"]) && $res["code"] == 200){
                return return_json(200, "登录");
            }else{
                return return_json(400, "未登录");
            }
        } else {
            return return_json(400, "未登录");
        }
    }
}
