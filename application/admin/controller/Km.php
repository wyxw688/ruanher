<?php

namespace app\admin\controller;

class Km extends Base
{
    public function index()
    {
        return $this->fetch();
    }

    public function GetKmlist()
    {
        $data = [
            "sort" => input('?sort') ? ''.input('sort') : 'id',
            "sortOrder" => input("?sortOrder") ? input("sortOrder") : "asc",
            "limit" => input('limit')?input('limit'):10,
            "page" => input('page')?input('page'):1,
            "km" => input("km"),
            "state" => input("state")
        ];
        $result = model("km")->getkmlist($data);
        return $result;
    }

    public function loginkm()
    {
        return $this->fetch();
    }

    public function GetloginKmlist()
    {
        $data = [
            "sort" => input('?sort') ? ''.input('sort') : 'id',
            "sortOrder" => input("?sortOrder") ? input("sortOrder") : "asc",
            "limit" => input('limit')?input('limit'):10,
            "page" => input('page')?input('page'):1,
            "km" => input("km"),
            "state" => input("state")
        ];
        $result = model("km")->getkmlist($data,1);
        return $result;
    }

    public function exportdownload()
    {
        $data = input("");
        $result = model("km")->exportdownload($data,$data["type"]);
        $txt = "";
        foreach ($result as $k => $v){
            $txt .= $v['km']."\r\n";
            // fwrite($file,$v['km']."\r\n");
        }
        return $this->success(md5(time()).".txt",'',$txt);
    }

    public function delete()
    {
        $id = explode(",", input('id'));
        $result = model("km")->deletekm($id);
        if($result == 1){
            return $this->success("删除成功");
        }else{
            return $this->error($result);
        }
    }

    public function add()
    {
        $data = input("");
        $result = model("km")->add($data);
        if($result == 1){
            return $this->success("添加成功(已剔除重复的值)");
        }else{
            return $this->error($result);
        }
    }

    public function addloginkm()
    {
        $data = input("");
        $result = model("km")->addloginkm($data);
        if($result == 1){
            return $this->success("添加成功(已剔除重复的值)");
        }else{
            return $this->error($result);
        }
    }

}