<?php

namespace app\admin\controller;

use think\Controller;

class Base extends Controller
{
    public function initialize()
    {
        //防止重复登陆
        if (!session("?admin")) {
            //判断是否选择了5天内免登录
            if (cookie("remembermedata.rememberme") == "1") {
                $remembermedata = cookie("remembermedata");
                $result = model('admin')->dologin($remembermedata);
                if (!$result) {
                    session(null);
                    cookie(null, 'think_');
                    $this->redirect("admin/login/index");
                }
            }else{
                session(null);
                cookie(null, 'think_');
                $this->redirect("admin/login/index");
            }
        }
    }
}
