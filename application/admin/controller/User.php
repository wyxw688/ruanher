<?php

namespace app\admin\controller;

use think\Db;

class User extends Base
{
    public function index()
    {
        return $this->fetch();
    }

    //获取所有用户
    public function getallusers()
    {
        $data = [
            "sort" => input('?sort') ? ''.input('sort') : 'id',
            "sortOrder" => input("?sortOrder") ? input("sortOrder") : "asc",
            "username" => input("username"),
            "limit" => input('limit')?input('limit'):10,
            "page" => input('page')?input('page'):1,
        ];
        $where = "id > 0";
        if(input("username") != ""){
            $where .= " and username like '%" . $data["username"] . "%'";
        }
        $data["where"] = $where;
        $result = model("user")->getallusers($data);
        return $result;
    }

    public function add()
    {
        $data = input("post.");
        $result = model("user")->add($data);
        if($result == 1){
            return $this->success("添加成功");
        }else{
            return $this->error($result);
        }
    }

    public function editstatus()
    {
        $id = explode(",", input('id'));
        $userstate = input("userstate");
        $result = model("user")->editstatus($id, $userstate);
        if ($result == 1) {
            return $this->success("修改成功");
        } else {
            return $this->error($result);
        }
    }

    public function delete()
    {
        $id = explode(",", input('id'));
        $result = model("user")->deleteuser($id);
        if ($result == 1) {
            return $this->success("删除成功");
        } else {
            return $this->error($result);
        }
    }

    public function edit()
    {
        if (request()->isAjax()) {
            $data = input();
            if (!isset($data["reasons"])) {
                $data["reasons"] = 0;
                $data["reasons_ban"] = "";
            } else {
                $data["reasons"] = 1;
            }
            $result = model("user")->edit($data);
            if ($result == 1) {
                return $this->success("修改成功");
            } else {
                return $this->error($result);
            }
        } else {
            $id = input("id");
            if ($id == "") {
                $this->error("服务器错误");
            }
            $result = model("user")->getuserinfo($id);
            if ($result == null) {
                $this->error("服务器错误");
            }
            $this->assign("user", $result);
            return $this->fetch();
        }
    }

}