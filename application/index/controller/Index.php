<?php

namespace app\index\controller;

use app\admin\model\Webset;
use app\admin\model\Appupdate;
use think\Controller;
use think\facade\Request;
use think\helper\Time;

class Index extends Controller
{
    public function index()
    {   
        $index = Webset::find();
        $app = Appupdate::find();
        $index['download'] = $app['download'];
        return $this->fetch()->assign('index',$index);
    }

    public function app($id)
    {
        $appinfo = db('appdown')
            ->alias("a")
            ->join("user u", "u.id=a.appuserid")
            ->join("appdownsort p", "p.id=a.appsortid")
            ->field("a.id,a.appname,a.appicon,a.appsortid,a.apppage,a.appmsg,a.appmod,a.apptop,a.appbb,a.appuserid,a.apptagid,a.appdata,a.apphenfu,a.appsce,a.appdownnum,a.appisvip,a.apptime,u.username,u.nickname,u.usertx,p.sortname")
            ->where("a.id", $id)
            ->find();
        $appinfo['appmsgs'] = db('webset')->field('rhtitle,rhlogo,rhappicon')->find();
        $randomApps = db("appdown")
        ->alias("a")
        ->join("user u", "u.id=a.appuserid")
        ->join("appdownsort p", "p.id=a.appsortid")
        ->field("a.id,a.appname,a.appicon,a.appsortid,a.apppage,a.appmsg,a.appmod,a.apptop,a.appbb,a.appuserid,a.apptagid,a.appdata,a.apphenfu,a.appsce,a.appdownnum,a.appisvip,a.apptime,u.username,u.nickname,u.usertx,p.sortname")
        ->where("a.appsortid", $appinfo["appsortid"])
        ->limit(5)
        ->orderRaw("RAND()")
        ->select();
        $tagIdArr = explode(",",$appinfo['apptagid']);
        foreach ($tagIdArr as $tagids => $tagid) {
            $apptag[$tagids]['tag'] = db('apptag')->where('id', $tagid)->value('tag');
        }
        foreach ($randomApps as $key => $value) {
            //评论量
            $randomApps[$key]["comment"] = db("comments")->where("appdownid", $value["id"])->count();

            //获取该软件评分人数
            $appstarnum = db("comments")->where("appdownid", $value["id"])->where("star","<",6)->count();
            //获取该软件评分数量的总分和
            $appallstar = db("comments")->where("appdownid", $value["id"])->where("star","<",6)->sum("star");
            //总分和除以评分人数得到最终评分
            if($appstarnum==0){
                $randomApps[$key]['appstar'] = 5;
            }else{
                $randomApps[$key]['appstar'] = ($appstarnum > 0) ? round($appallstar / $appstarnum, 2) : 0;
            }
        }
        //获取该软件评分人数
        $appstarnum = db("comments")->where("appdownid", $id)->where("star","<",6)->count();
        //获取该软件评分数量的总分和
        $appallstar = db("comments")->where("appdownid", $id)->where("star","<",6)->sum("star");
        //总分和除以评分人数得到最终评分
        if($appstarnum==0){
            $appinfo['star'] = 5;
        }else{
            $appinfo['star'] = ($appstarnum > 0) ? round($appallstar / $appstarnum, 2) : 0;
        }
        $appinfo['appstarnum']=$appstarnum;
        
        $scearr = explode(",",$appinfo['appsce']);
        foreach ($scearr as $acekey => $sceurl) {
            $appsce[$acekey]['link'] = $sceurl;
        }
        $appinfo['appdownurls']=db('appupdate')->order("time","desc")->field('versioncode,content,download,time,silentup,forceup,wifiup')->find();
        $this->assign('apptag', $apptag);
        $this->assign('appsce', $appsce);
        $this->assign('app', $appinfo);
        return $this->fetch();
    }
}
