<?php

namespace app\http\middleware;

use app\admin\service\SystemLogService;
use think\Db;

class AdminAuth
{
    protected $_request;
    protected $_insertData = [];
    public function handle($request, \Closure $next)
    {
        // 添加中间件执行代码
        $this->_request = $request;
        $this->_insertData['admin_id'] = session('admin.id');
        $this->_insertData['admin_name'] = session('admin.adminname');
        $this->_insertData['url'] = $request->url();
        $this->_insertData['method'] = $request->method();
        $this->_insertData['ip'] = $request->server('REMOTE_ADDR');
        $this->_insertData['useragent'] = $request->header()["user-agent"];
        $this->_insertData['create_time'] = time();
        if ($request->method() == "POST") {
            $this->_insertData['content'] = json_encode($request->post(),JSON_UNESCAPED_UNICODE);
            SystemLogService::instance()->save($this->_insertData);
        }
        return $next($request);
    }
}
