# 软盒后台管理系统

#### 介绍
一个开源的软件库系统


#### 更新说明
更新日期：2023.07.24
v1.0.8.23724
1. 修复部分接口
2. 删除根据标签获取软件列表接口，整合到通用列表接口

更新日期：2023.07.21
v1.0.7 23721.3
1. 修复上传接口和软件发布接口

更新日期：2023.07.21
v1.0.7 23721.2
1. 新增上传图片接口
2. 修复部分发布软件问题

更新日期：2023.07.21
v1.0.7 23721
1. 修复支付回调接口问题
2. 修复编辑用户VIP到期时间不显示问题

更新日期：2023.07.12
v1.0.6 23712.2
1. 修复发布评分不能小数点BUG

更新日期：2023.07.12
v1.0.6 23.7.12.1
1. 修复后台BUG
2. 修复软件分析WEB页面BUG
3. 其他

更新日期：2023.07.12
v1.0.6 23.7.12
1. 修复后台添加软件精选反向BUG
2. 新增软件分享WEB界面
3. 其他

更新日期：2023.07.08
v1.0.5 23.7.5-2
1. 优化获取会员列表接口

更新日期：2023.07.08
v1.0.5 23.7.5-1
1. 修复获取广告列表报错问题

更新日期：2023.07.08
v1.0.5 23.7.5
1. 去除无用代码
2. 新增后台设置广告功能，新增广告接口
需要执行SQL：
CREATE TABLE IF NOT EXISTS `rh_appad` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `adname` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '软盒系统',
  `adlink` varchar(1024) COLLATE utf8_unicode_ci DEFAULT 'https://www.ruanher.com',
  `adimgurl` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adtype` int(11) DEFAULT '0' COMMENT '0为首页广告，1为分类页广告',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `rh_appad` (`id`, `adname`, `adlink`, `adimgurl`, `adtype`) VALUES
(1, '软盒软件库', 'https://www.ruanher.com', 'http://demo.ruanher.com/uploads/images/20230708/f6581bf3378bb8032fbb48d360daabf3.png', 1),
(2, '软盒软件库', 'https://www.ruanher.com', 'http://demo.ruanher.com/uploads/images/20230708/f6581bf3378bb8032fbb48d360daabf3.png', 0);

3. 修复了部分BUG

更新日期：2023.07.04
V1.0.4 23.7.4-1
1. 修复注册用户提示邮箱为空
2. 修复上传头像接口问题

更新日期：2023.07.04
V1.0.4 23.7.4
1. 修复软件截图超过4张会出现错误问题
2. 修复新增软件和编辑软件填写的介绍不保存问题
3. 修复APP发布的新版本删除错误问题
4. 修复后台添加用户，APP无法登陆问题

此次更新执行SQL
ALTER TABLE `rh_appdown` CHANGE `appsce` `appsce` VARCHAR(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '软件截图';
ALTER TABLE `rh_appdown` CHANGE `appmsg` `appmsg` VARCHAR(2550) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '软件介绍'; 

-------------------------------------

更新日期：2023.07.02
V1.0.3 23.7.2-2
1. 修复后台上传软件只有发火狐浏览器能上传问题

更新日期：2023.07.02
V1.0.3 23.7.2-1
1. 修复后台修改用户密码错误问题
2. 修复发送注册验证码填写邮箱问题

更新日期：2023.07.02
V1.0.2 23.7.2
1. 修复全新安装导致后台出现的问题
2. 修复后台登录注册配置出现的问题

-------------------------------------

更新日期：2023.07.01
V1.0.2 23.7.1-1
1. 修复搜索/分类，软件列表接口不输出星级

更新日期：2023.07.01
V1.0.2 23.7.1
1. 修复部分接口请求问题
2. 新增部分接口

更新日期：2023.06.29
V1.0.1 23.6.29-3
1.  修复所有接口的不传参问题

更新日期：2023.06.29
V1.0.1 23.6.29-2
1.  完善接口加密功能

更新日期：2023.06.29
V1.0.1 23.6.29
1.  修复后台无法删除更新版本的BUG
2.  新增TAG获取软件列表
3.  文章详情页接口新增推荐软件

此版本更新请执行SQL：
ALTER TABLE `rh_app` DROP COLUMN `is_reg`;
ALTER TABLE `rh_app` DROP COLUMN `equipment_num`;


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
