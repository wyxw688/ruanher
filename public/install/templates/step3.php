<!doctype html>
<html>
<head>
    <meta charset="UTF-8"/>
    <title><?php echo $Title; ?> - <?php echo $Powered; ?></title>
    <link rel="stylesheet" href="./css/install.css?v=9.0"/>
    <link rel="stylesheet" href="./css/step3.css"/>
    <!-- 引入样式 -->
    <link rel="stylesheet" href="./css/theme-chalk.css">
    <!-- import Vue before Element -->
    <script src="./js/vue2.6.11.js"></script>
    <!-- import JavaScript -->
    <script src="./js/element-ui.js?v=9.0"></script>
</head>
<body>
<div class="wrap" id="step3">
    <div class="title">
        创建数据
    </div>
    <section class="section">
        <form id="J_install_form" action="index.php?step=4" method="post">
            <div class="server"  ref="mianscroll">
                <table width="100%">
                    <tr>
                        <td class="td1" width="100">数据库信息</td>
                        <td class="td1" width="200">&nbsp;</td>
                        <td class="td1">&nbsp;</td>
                    </tr>

                    <tr>
                        <td class="tar">数据库用户名：</td>
                        <td><input type="text" name="dbuser" id="dbuser" value="root" class="input"></td>
                        <td>
                            <div id="J_install_tip_dbuser"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tar">数据库密码：</td>
                        <td><input type="password" name="dbpw" id="dbpw" value="" class="input" autoComplete="off"></td>
                        <td>
                            <div id="J_install_tip_dbpw"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tar">数据库名：</td>
                        <td><input type="text" name="dbname" id="dbname" value="ruanher" class="input"></td>
                        <td>
                            <div id="J_install_tip_dbname"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tar">数据库服务器：</td>
                        <td><input type="text" name="dbhost" id="dbhost" value="127.0.0.1" class="input"></td>
                        <td>
                            <div id="J_install_tip_dbhost"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tar">数据库端口：</td>
                        <td><input type="text" name="dbport" id="dbport" value="3306" class="input"
                                   onBlur="mysqlDbPwd(0)"></td>
                        <td>
                            <div id="J_install_tip_dbport"></div>
                        </td>
                    </tr>

                    <tr>
                        <td class="tar">数据库表前缀：</td>
                        <td><input type="text" name="dbprefix" id="dbprefix" value="rh_" class="input"></td>
                        <td></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td class="td1" width="100">管理员信息</td>
                        <td class="td1" width="200">&nbsp;</td>
                        <td class="td1">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tar">管理员帐号：</td>
                        <td><input type="text" name="adminname" id="adminname" value="admin" class="input"
                                   onblur="checkForm()"></td>
                        <td>
                            <div id="J_install_tip_manager"></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tar">管理员密码：</td>
                        <td><input type="password" name="adminpass" id="adminpass" class="input" autoComplete="off"
                                placeholder="请输入密码(至少6个字符)"  placeholder-class="pl-style" onblur="checkForm()">
                        </td>
                        <td>
                            <div id="J_install_tip_manager_pwd"><span class="gray">请输入至少6个字符密码</span></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="tar">重复密码：</td>
                        <td><input type="password" name="manager_ckpwd" id="manager_ckpwd" class="input"
                                   autoComplete="off" placeholder="请再次输入密码" onkeyup="checkForm()"></td>
                        <td>
                            <div id="J_install_tip_manager_ckpwd"></div>
                        </td>
                    </tr>

                </table>
        
            </div>
            <div class="bottom-btn">
                <div class="bottom tac up-btn">
                    <a href="./index.php?step=2" class="btn">上一步</a>
                </div>
                <div class="bottom tac next">
                    <a @click="submitForm();" class="btn">下一步</a>
                </div>
            </div>
        </form>
    </section>
    <div style="width:0;height:0;overflow:hidden;"><img src="./images/install/pop_loading.gif"></div>
    <script src="./js/jquery.js?v=9.0"></script>
    <script src="./js/validate.js?v=9.0"></script>
    <script src="./js/ajaxForm.js?v=9.0"></script>
    <script>
        //验证管理员信息
        function checkForm() {
            let adminname = $.trim($('#adminname').val());				//用户名表单
            let adminpass = $.trim($('#adminpass').val());				//密码表单
            let manager_ckpwd = $.trim($('#manager_ckpwd').val());		//密码提示区
            if (adminname.length == 0) {
                $('#J_install_tip_manager').html('<span for="dbname" generated="true" class="tips_error" style="">请输入管理账号</span>');
                return false;
            }
            if (!(/^[a-zA-Z]{0,}$/.test(adminname))) {
                $('#J_install_tip_manager').html('<span generated="true" class="tips_error" style="">账号必须为英文或者数字</span>');
                return false;
            } else {
                $('#J_install_tip_manager').html('<span generated="true" class="tips_success" style="">用户名可用</span>');
            }
            if (adminpass.length < 6) {
                $('#J_install_tip_manager_pwd').html('<span for="dbname" generated="true" class="tips_error" style="">管理员密码必须5位数以上</span>');
                return false;
            } else {
                $('#J_install_tip_manager_pwd').html('<span generated="true" class="tips_success" style="">密码可用</span>');
            }
            if (manager_ckpwd != adminpass) {
                $('#J_install_tip_manager_ckpwd').html('<span for="dbname" generated="true" class="tips_error" style="">两次密码不一致</span>');
                return false;
            } else {
                $('#J_install_tip_manager_ckpwd').html('<span generated="true" class="tips_success" style="">密码正确</span>');
            }
            return true;
        }
        new Vue({
            el: '#step3',
            data() {
                return {value: false, radio: 0}
            },
            created() {

            },
            methods: {
                mysqlDbPwd() {
                    let data = {
                        'dbHost': $('#dbhost').val(),
                        'dbUser': $('#dbuser').val(),
                        'dbPwd': $('#dbpw').val(),
                        'dbName': $('#dbname').val(),
                        'dbport': $('#dbport').val()
                    };
                    let url = "<?php echo $_SERVER['PHP_SELF']; ?>?step=3&mysqldbpwd=1";
                    return new Promise((resolve, reject) => {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: data,
                            dataType: 'JSON',
                            success: (msg) => {
                                resolve(msg);
                            },
                            error: (err) => {
                                reject(err)
                            }
                        });
                    })

                },



                jumpButton(){
                   this.$refs.mianscroll.scrollTop = this.$refs.mianscroll.clientHeight
                },
                submitForm() {
                    this.mysqlDbPwd().then(res => {
                        if (res == 2002) {
                            this.value = true
                            $('#J_install_tip_dbhost').html('<span for="dbname" generated="true" class="tips_error" >地址或端口错误</span>');
                            $('#J_install_tip_dbport').html('<span for="dbname" generated="true" class="tips_error" >地址或端口错误</span>');
                            return false;
                        } else if (res == -1) {
                            $('#J_install_tip_dbhost').html('');
                            $('#J_install_tip_dbport').html('');
                            $('#J_install_tip_dbname').html('<span for="dbname" generated="true" class="tips_error" >数据库链接配置失败</span>');
                            return false;
                        } else if (res == -2) {
                            $('#J_install_tip_dbhost').html('');
                            $('#J_install_tip_dbport').html('');
                            $('#J_install_tip_dbname').html('<span for="dbname" generated="true" class="tips_error" >请在mysql配置文件修sql-mode或sql_mode为NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION</span><a href="https://doc.crmeb.com/web/single/crmeb_v4/936" target="_blank">查看文档</a>');
                            return false;
                        } else if (res == 1045) {
                            $('#J_install_tip_dbhost').html('');
                            $('#J_install_tip_dbport').html('');
                            $('#J_install_tip_dbname').html('');
                            $('#J_install_tip_dbuser').html('<span for="dbname" generated="true" class="tips_error" >用户名或密码错误</span>');
                            $('#J_install_tip_dbpw').html('<span for="dbname" generated="true" class="tips_error" >用户名或密码错误</span>');
                            return false;
                        } else if (res == -4) {
                            $('#J_install_tip_dbhost').html('');
                            $('#J_install_tip_dbport').html('');
                            $('#J_install_tip_dbuser').html('');
                            $('#J_install_tip_dbpw').html('');
                            $('#J_install_tip_dbname').html('<span for="dbname" generated="true" class="tips_error" >无权限创建数据，请先手动创建数据库</span>');
                            return false;
                        } else if (res == -3) {
                            $('#J_install_tip_dbhost').html('');
                            $('#J_install_tip_dbport').html('');
                            $('#J_install_tip_dbuser').html('');
                            $('#J_install_tip_dbpw').html('');
                            $('#J_install_tip_dbname').html('<span for="dbname" generated="true" class="tips_error" >数据库不为空，请更换一个数据库</span>');
                            return false;
                        } else if (res == -5) {
                            $('#J_install_tip_dbhost').html('');
                            $('#J_install_tip_dbport').html('');
                            $('#J_install_tip_dbuser').html('');
                            $('#J_install_tip_dbpw').html('');
                            $('#J_install_tip_dbname').html('<span for="dbname" generated="true" class="tips_error" >MySql数据库必须是5.6及以上版本</span>');
                            return false;
                        } else if (res == 1) {
                            $('#J_install_tip_dbhost').html('');
                            $('#J_install_tip_dbport').html('');
                            $('#J_install_tip_dbuser').html('');
                            $('#J_install_tip_dbpw').html('');
                            $('#J_install_tip_dbname').html('<span generated="true" class="tips_success" style="">数据库配置成功</span>');
                        } else {
                            $('#J_install_tip_dbhost').html('');
                            $('#J_install_tip_dbport').html('');
                            $('#J_install_tip_dbuser').html('');
                            $('#J_install_tip_dbpw').html('');
                            $('#J_install_tip_dbname').html('<span for="dbname" generated="true" class="tips_error" >未知错误</span>');
                            return false;
                        }
                        
                            if (checkForm()) {
                                $("#J_install_form").submit(); // ajax 验证通过后再提交表单
                            }
                    }).catch(err => {
                        $('#J_install_tip_dbhost').html('');
                        $('#J_install_tip_dbport').html('');
                        $('#J_install_tip_dbuser').html('');
                        $('#J_install_tip_dbpw').html('');
                        $('#J_install_tip_dbname').html('<span for="dbname" generated="true" class="tips_error" >未知错误1</span>');
                        return false;
                    })
                }
            }
        })


    </script>
</div>
<?php require './templates/footer.php'; ?>
</body>
</html>
