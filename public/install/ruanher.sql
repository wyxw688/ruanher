-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2023-06-27 23:36:44
-- 服务器版本： 5.7.40-log
-- PHP 版本： 7.4.33

CREATE TABLE `rh_admin` (
  `id` int(11) NOT NULL COMMENT '主键id',
  `adminname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '管理员用户名',
  `adminpass` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '管理员密码',
  `adminqq` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'qq',
  `nickname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '昵称',
  `admintoken` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '秘钥'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;



CREATE TABLE `rh_app` (
  `id` int(11) NOT NULL COMMENT 'id',
  `rhqq` varchar(255) COLLATE utf8_unicode_ci DEFAULT '779259529' COMMENT '客服qq',
  `rhqun` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'j_wUPZb-dYJ-mhRsGlZ7_mq4wyW0lrcK' COMMENT '官方群组',
  `zc_money` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '注册赠送金币',
  `zc_vip` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '注册赠送会员',
  `sign_money` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '签到赠送金币',
  `sign_vip` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '签到赠送会员',
  `invitation_money` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '邀请人获得金币',
  `invitation_vip` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0' COMMENT '邀请人获得会员',
  `signkey` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1212121212121212' COMMENT '接口加密秘钥',
  `signiv` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1212121212121212' COMMENT '接口加密偏移值',
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT '779259529@qq.com' COMMENT '发件邮箱',
  `emailpass` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发件邮箱授权码',
  `emailport` varchar(255) COLLATE utf8_unicode_ci DEFAULT '465' COMMENT '发件邮箱端口',
  `emailname` varbinary(255) DEFAULT '软盒' COMMENT '发件人名称',
  `emailsite` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'smtp.qq.com' COMMENT 'SMTP服务器地址',
  `payapi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '第三方支付API',
  `payid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商户ID',
  `paykey` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商户秘钥',
  `aliid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '支付宝ID',
  `aliprivatekey` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '支付宝私钥',
  `alikey` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '支付宝公钥',
  `paynotify` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '异步通知地址',
  `appmsg` varchar(255) COLLATE utf8_unicode_ci DEFAULT '软盒系统正式发布' COMMENT '应用公告',
  `appview` int(11) DEFAULT '0' COMMENT '应用访问量'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

INSERT IGNORE INTO `rh_app` (`id`, `rhqq`, `rhqun`, `zc_money`, `zc_vip`, `sign_money`, `sign_vip`, `invitation_money`, `invitation_vip`, `signkey`, `signiv`, `email`, `emailpass`, `emailport`, `emailname`, `emailsite`, `payapi`, `payid`, `paykey`, `aliid`, `aliprivatekey`, `alikey`, `paynotify`, `appmsg`, `appview`) VALUES
	(1, '779259529', 'j_wUPZb-dYJ-mhRsGlZ7_mq4wyW0lrcK', '1', '2', '0', '0', '0', '1', '1212121212121212', '3434343434343435', '779259529@qq.com', '540b32ae6ac88d87', '465', _binary 0xE8BDAFE79B92, 'smtp.sina.cn', 'http://www.ruanher.com/', '123456', '123456', 'test111', 'test11111', 'test111', 'http://www.ruanher.com/api/Paynotify', '6666', 3);

--
-- 表的结构 `rh_appdown`
--

CREATE TABLE `rh_appdown` (
  `id` bigint(20) NOT NULL,
  `appname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件名称',
  `appsortid` int(11) DEFAULT '1' COMMENT '软件分类',
  `appicon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件图标',
  `apppage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件包名',
  `appmsg` varchar(2550) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件介绍',
  `appmod` tinyint(4) DEFAULT '0' COMMENT '0原版1破解版',
  `apptop` tinyint(4) DEFAULT '0' COMMENT '0不推送1精选',
  `appbb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件版本号',
  `appuserid` int(11) DEFAULT NULL COMMENT '软件作者',
  `appdata` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件大小',
  `apphenfu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件横幅图',
  `appsce` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件截图',
  `appdownnum` int(11) DEFAULT '0' COMMENT '软件下载数量',
  `appisvip` tinyint(4) DEFAULT '0' COMMENT '0免费1会员',
  `appdownurl` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'https://www.ruanher.com' COMMENT '下载地址',
  `apptagid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件标签',
  `apppost` tinyint(4) DEFAULT '0' COMMENT '0为未审核1为已发布2为违规',
  `appwgmsg` varchar(255) COLLATE utf8_unicode_ci DEFAULT '推广引流APP' COMMENT '违规理由',
  `apptime` datetime DEFAULT NULL COMMENT '更新时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `rh_appdownsort`
--

CREATE TABLE `rh_appdownsort` (
  `id` bigint(20) NOT NULL,
  `sortname` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分类名称',
  `sorticon` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '分类图标'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `rh_appdownsort`
--

INSERT INTO `rh_appdownsort` (`id`, `sortname`, `sorticon`) VALUES
(1, '默认分类', 'http://demo.ruanher.com/uploads/images/20230627/89cca5fb80296e4d0ac0abab9168a009.png');

-- --------------------------------------------------------

--
-- 表的结构 `rh_apptag`
--

CREATE TABLE `rh_apptag` (
  `id` int(11) NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


--
-- 表的结构 `rh_appupdate`
--

CREATE TABLE `rh_appupdate` (
  `id` int(11) NOT NULL,
  `versioncode` varchar(255) COLLATE utf8_unicode_ci DEFAULT '1.0' COMMENT '版本号',
  `content` longtext COLLATE utf8_unicode_ci COMMENT '更新内容',
  `download` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '下载地址',
  `time` datetime DEFAULT NULL COMMENT '发布时间',
  `silentup` tinyint(4) DEFAULT '0' COMMENT '更新方式0弹窗更新1静默更新',
  `forceup` tinyint(4) DEFAULT '0' COMMENT '是否强制更新',
  `wifiup` tinyint(4) DEFAULT '0' COMMENT '是否非WIFI提示'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `rh_appupdate`
--

INSERT INTO `rh_appupdate` (`id`, `versioncode`, `content`, `download`, `time`, `silentup`, `forceup`, `wifiup`) VALUES
(1, '1.0', '软盒第一个版本', 'https://www.ruanher.com', '2023-06-27 23:36:36', 0, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `rh_comments`
--

CREATE TABLE `rh_comments` (
  `id` int(11) NOT NULL,
  `parentid` int(11) DEFAULT NULL COMMENT '父评论的id,如果不是对评论的回复,那么该值为0',
  `appdownid` int(11) DEFAULT NULL COMMENT '软件id',
  `content` text COLLATE utf8_unicode_ci COMMENT '内容',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `star` float NOT NULL DEFAULT '6' COMMENT '评分',
  `time` datetime DEFAULT NULL COMMENT '时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;


--
-- 表的结构 `rh_invitation`
--

CREATE TABLE `rh_invitation` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL COMMENT '邀请人  用户id',
  `newuserid` int(11) DEFAULT NULL COMMENT '被邀请  用户id',
  `createtime` datetime DEFAULT NULL COMMENT '邀请时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;


--
-- 表的结构 `rh_km`
--

CREATE TABLE `rh_km` (
  `id` int(11) NOT NULL,
  `km` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `money` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '金币',
  `viptime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '会员天数',
  `device` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '设备号',
  `expire` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '过期时间',
  `state` tinyint(1) DEFAULT '0' COMMENT '0未使用1已使用',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '使用者',
  `usetime` datetime DEFAULT NULL COMMENT '使用时间',
  `creattime` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- 表的结构 `rh_order`
--

CREATE TABLE `rh_order` (
  `id` int(11) NOT NULL,
  `order_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '订单号',
  `shop_id` int(11) DEFAULT NULL COMMENT '商品id',
  `transaction_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '交易号',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `status` tinyint(1) DEFAULT '0' COMMENT '0待付款1已付款',
  `paymenttime` datetime DEFAULT NULL COMMENT '支付时间',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '价格'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- 表的结构 `rh_shop`
--

CREATE TABLE `rh_shop` (
  `id` int(11) NOT NULL COMMENT 'Id',
  `name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `detail` varchar(255) DEFAULT NULL COMMENT '商品详情',
  `price` varchar(255) DEFAULT NULL COMMENT '价格',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `vipdate` int(11) DEFAULT NULL COMMENT '会员天数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `rh_shop`
--

INSERT INTO `rh_shop` (`id`, `name`, `detail`, `price`, `createtime`, `vipdate`) VALUES
(1, '体验天卡', '会员一天体验卡', '0.1', '2023-03-22 14:50:17', 3);

-- --------------------------------------------------------

--
-- 表的结构 `rh_sign`
--

CREATE TABLE `rh_sign` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `lasttime` int(11) DEFAULT NULL COMMENT '最后一次签到时间',
  `series_days` int(11) DEFAULT '0' COMMENT '累计签到天数',
  `continuity_days` int(11) DEFAULT '0' COMMENT '连续签到天数'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

--
-- 表的结构 `rh_system_log`
--

CREATE TABLE `rh_system_log` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `admin_id` int(10) UNSIGNED DEFAULT '0' COMMENT '管理员ID',
  `admin_name` varchar(255) NOT NULL DEFAULT '' COMMENT '管理员name',
  `url` varchar(1500) NOT NULL DEFAULT '' COMMENT '操作页面',
  `method` varchar(50) NOT NULL COMMENT '请求方法',
  `content` text NOT NULL COMMENT '内容',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) DEFAULT '' COMMENT 'User-Agent',
  `create_time` int(10) DEFAULT NULL COMMENT '操作时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台操作日志表' ROW_FORMAT=COMPACT;


--
-- 表的结构 `rh_user`
--

CREATE TABLE `rh_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '密码',
  `usertx` varchar(255) COLLATE utf8_unicode_ci DEFAULT '/user.png' COMMENT '头像',
  `nickname` varchar(255) COLLATE utf8_unicode_ci DEFAULT '这个人暂未设置昵称' COMMENT '昵称',
  `money` int(255) DEFAULT '0' COMMENT '金币',
  `viptime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '会员时间戳',
  `sex` tinyint(255) DEFAULT '0' COMMENT '0男1女',
  `useremail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `signature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '个性签名',
  `invitecode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邀请码',
  `reasons` tinyint(1) DEFAULT '0' COMMENT '状态0正常1封禁',
  `reasons_ban` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '封禁理由',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '登录ip',
  `usertoken` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '验证作用',
  `userbg` varchar(255) COLLATE utf8_unicode_ci DEFAULT '/userbg.jpg' COMMENT '背景'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `rh_user`
--

INSERT INTO `rh_user` (`id`, `username`, `password`, `usertx`, `nickname`, `money`, `viptime`, `sex`, `useremail`, `signature`, `invitecode`, `reasons`, `reasons_ban`, `create_time`, `ip`, `usertoken`, `userbg`) VALUES
(1, 'yuncheng', 'fcea920f7412b5da7be0cf42b8c93759', 'http://demo.ruanher.com/uploads/images/20230627/89cca5fb80296e4d0ac0abab9168a009.png', '软盒系统', 0, '1687883016', 0, '779259529@qq.com', '我是个性签名', 'brUlFe7b7', 0, '', '2023-03-22 13:11:51', '127.0.0.1', 'd75757fc897e4bfcfa1e59fad4199492', 'http://demo.ruanher.com/uploads/images/20230627/userbg.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `rh_webset`
--

CREATE TABLE `rh_webset` (
  `webid` int(11) NOT NULL,
  `rhtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT '软盒' COMMENT '网站名称',
  `rhkeywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT '软件库源码,uniapp软件库源码,免费软件库源码,软盒软件库源码,云程工作室,葫芦芥子博客,云程软盒' COMMENT '网站关键词',
  `rhdescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT '软盒为搭建提供开源免费的软件库系统，助力小白搭建自己的软件库系统，我们提供软件库后台+前台源码服务' COMMENT '网站介绍',
  `rhlogo` varchar(255) COLLATE utf8_unicode_ci DEFAULT '网站LOGO' COMMENT '网站LOGO',
  `rhlogo1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '网站LOGO1',
  `rhindeximg` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件第一页截图',
  `rhtabimg1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件首屏截图',
  `rhtabimg2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件TAB2截图',
  `rhtabimg3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件TAB3截图',
  `rhappimg1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件截图',
  `rhappimg2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件截图',
  `rhappimg3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件截图',
  `rhappimg4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件截图',
  `rhappimg5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件截图',
  `rhappimg6` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件截图',
  `rhappimg7` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件截图',
  `rhappimg8` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件截图',
  `rhmsg` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '关于我们',
  `rhappicon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '软件图标',
  `rhbeian` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '网站备案号',
  `rhqq` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客服QQ',
  `rhqun` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '官方群聊'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='网站首页下载页';

--
-- 转存表中的数据 `rh_webset`
--

INSERT INTO `rh_webset` (`webid`, `rhtitle`, `rhkeywords`, `rhdescription`, `rhlogo`, `rhlogo1`, `rhindeximg`, `rhtabimg1`, `rhtabimg2`, `rhtabimg3`, `rhappimg1`, `rhappimg2`, `rhappimg3`, `rhappimg4`, `rhappimg5`, `rhappimg6`, `rhappimg7`, `rhappimg8`, `rhmsg`, `rhappicon`, `rhbeian`, `rhqq`, `rhqun`) VALUES
(1, '软盒', '软件库源码,uniapp软件库源码,免费软件库源码,软盒软件库源码,云程工作室,葫芦芥子博客,云程软盒', '软盒为搭建提供开源免费的软件库系统，助力小白搭建自己的软件库系统，我们提供软件库后台+前台源码服务', 'http://demo.ruanher.com/uploads/images/20230628/7ea5381484e8af7456d83b90c076181a.png', 'http://demo.ruanher.com/uploads/images/20230628/f99c48f40bb16e6d91c59b8bf5878835.png', 'http://demo.ruanher.com/uploads/images/20230628/625810aafcb13258c0898b6d3a6ec497.png', 'http://demo.ruanher.com/uploads/images/20230627/cdac1622ead4cca1e0dfa99519ae0df1.png', 'http://demo.ruanher.com/uploads/images/20230627/cdac1622ead4cca1e0dfa99519ae0df1.png', 'http://demo.ruanher.com/uploads/images/20230627/cdac1622ead4cca1e0dfa99519ae0df1.png', 'http://demo.ruanher.com/uploads/images/20230627/cdac1622ead4cca1e0dfa99519ae0df1.png', 'http://demo.ruanher.com/uploads/images/20230627/cdac1622ead4cca1e0dfa99519ae0df1.png', 'http://demo.ruanher.com/uploads/images/20230627/cdac1622ead4cca1e0dfa99519ae0df1.png', 'http://demo.ruanher.com/uploads/images/20230627/cdac1622ead4cca1e0dfa99519ae0df1.png', 'http://demo.ruanher.com/uploads/images/20230627/cdac1622ead4cca1e0dfa99519ae0df1.png', 'http://demo.ruanher.com/uploads/images/20230627/cdac1622ead4cca1e0dfa99519ae0df1.png', 'http://demo.ruanher.com/uploads/images/20230627/cdac1622ead4cca1e0dfa99519ae0df1.png', 'http://demo.ruanher.com/uploads/images/20230627/cdac1622ead4cca1e0dfa99519ae0df1.png', '由软盒（www.ruanher.com）提供服务，一个软件库系统！！！', 'http://demo.ruanher.com/uploads/images/20230627/89cca5fb80296e4d0ac0abab9168a009.png', '779259529', 'http://wpa.qq.com/msgrd?v=3&uin=779259529&site=qq&menu=yes', 'http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=ROH8S_EDLqCMIF8hmaTy_aT16hW-tPsv&authKey=iICOC7iKrUv3IK3Cyjl0TjXBYqrqAR%2FcPtHv5UDv%2BPbQJGM2FdoAdI5dmqD5PzRR&noverify=0&group_code=235492833');


CREATE TABLE IF NOT EXISTS `rh_appad` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `adname` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '软盒系统',
  `adlink` varchar(1024) COLLATE utf8_unicode_ci DEFAULT 'https://www.ruanher.com',
  `adimgurl` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adtype` int(11) DEFAULT '0' COMMENT '0为首页广告，1为分类页广告',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `rh_appad` (`id`, `adname`, `adlink`, `adimgurl`, `adtype`) VALUES
(1, '软盒软件库', 'https://www.ruanher.com', 'http://demo.ruanher.com/uploads/images/20230708/f6581bf3378bb8032fbb48d360daabf3.png', 1),
(2, '软盒软件库', 'https://www.ruanher.com', 'http://demo.ruanher.com/uploads/images/20230708/f6581bf3378bb8032fbb48d360daabf3.png', 0);
--
-- 转储表的索引
--

--
-- 表的索引 `rh_appad`
--
ALTER TABLE `rh_appad`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `rh_admin`
--
ALTER TABLE `rh_admin`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `rh_app`
--
ALTER TABLE `rh_app`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `rh_appdown`
--
ALTER TABLE `rh_appdown`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `rh_appdownsort`
--
ALTER TABLE `rh_appdownsort`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `rh_apptag`
--
ALTER TABLE `rh_apptag`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `rh_appupdate`
--
ALTER TABLE `rh_appupdate`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `rh_comments`
--
ALTER TABLE `rh_comments`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `rh_expand`
--
ALTER TABLE `rh_expand`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `rh_invitation`
--
ALTER TABLE `rh_invitation`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `newuserid` (`newuserid`) USING BTREE;

--
-- 表的索引 `rh_km`
--
ALTER TABLE `rh_km`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `rh_order`
--
ALTER TABLE `rh_order`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `userid` (`userid`) USING BTREE;

--
-- 表的索引 `rh_shop`
--
ALTER TABLE `rh_shop`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 表的索引 `rh_sign`
--
ALTER TABLE `rh_sign`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `userid` (`userid`) USING BTREE;

--
-- 表的索引 `rh_system_log`
--
ALTER TABLE `rh_system_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `rh_user`
--
ALTER TABLE `rh_user`
  ADD PRIMARY KEY (`id`) USING BTREE;

ALTER TABLE `rh_webset`
  ADD PRIMARY KEY (`webid`),
  ADD UNIQUE KEY `webid` (`webid`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `rh_app`
--
ALTER TABLE `rh_app`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `rh_appdown`
--
ALTER TABLE `rh_appdown`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `rh_appdownsort`
--
ALTER TABLE `rh_appdownsort`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `rh_apptag`
--
ALTER TABLE `rh_apptag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `rh_appupdate`
--
ALTER TABLE `rh_appupdate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `rh_comments`
--
ALTER TABLE `rh_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `rh_expand`
--
ALTER TABLE `rh_expand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `rh_invitation`
--
ALTER TABLE `rh_invitation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `rh_km`
--
ALTER TABLE `rh_km`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `rh_order`
--
ALTER TABLE `rh_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `rh_shop`
--
ALTER TABLE `rh_shop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `rh_sign`
--
ALTER TABLE `rh_sign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `rh_system_log`
--
ALTER TABLE `rh_system_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- 使用表AUTO_INCREMENT `rh_user`
--
ALTER TABLE `rh_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
